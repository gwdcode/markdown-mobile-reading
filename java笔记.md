------

作者：李连宇　邮箱:llybjd@163.com

------

https://gitee.com/seven_hundred_and_eightysix/javaSe_2021_6.git

# 一、java入门

## 1.java简介

### 1.1 什么是程序

是一组用计算机语言编写的指令序列的集合,运行于计算机上，用来满足人们某种需求。

比如"1101"这条指令，在windows上可规定为打开记事本。

### 1.2　java的最大优势是什么

可以作到跨平台

#### 1.2.1 什么是跨平台

一次编译，到处运行 

#### 1.2.2 java跨平台背景：

不同平台的系统，其指令集不同：

windows系统：

1101--打开记事本

1100--打开office 

linux系统:

1001--打开记事本

1011--打开office

#### 1.2.3 java为什么跨平台

+ C语言为什么不跨平台

  ![跨平台1](picture\跨平台01.png)


+ java如何作到跨平台

 ![跨平台](picture\跨平台02.png)



## 2.JDK安装

### 2.1 下载jdk

```
https://www.oracle.com/
https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
网盘下载链接：https://pan.baidu.com/s/13VObC_awUmCKNiDohQOgAQ 
提取码：jnge
```

### 2.2 jdk包含什么

+ jre:虚拟机和运行虚拟机所需要的支持文件
  + jvm:虚拟机
  + 支持文件
+ 开发工具类和编译器

### 2.3 环境变量

jdk>jre>jvm

java_home:用来说明jdk在什么位置:C:\Program Files\Java\jdk1.8.0_91

path:用来指定可执行文件在什么位置，以方便系统找到该文件

​	C:\Program Files\Java\jdk1.8.0_91\bin

classpath:用来告诉虚拟机，从什么位置能找到.class文件,如果没有配置，则默认从当前路径下找class文件，如果配置了classpath,则只会从指定位置找.class文件.

.;d:\work----注意，这是两个路径，第一个路径.表示当前工作目录



### 2.4 java的三种开发技术

+ JavaSE:标准版
+ JavaEE：企业版
+ JavaME：Micro Edition

## 3.第一个程序

### 1.在指定位置编译

​	把工作路径切换到Hello.java文件所在位置

​	javac Hello.java--->会在当前目录中生成Hello.class文件

​	java Hello---->会在classpath指定目录中找Hello.class文件

### 2.在任意位置编译，任意位置运行

javac -d d:/work/tmp d:/work/Hello.java----->在任意位置编译指定的一个文件，并把生成的class文件放在指定的位置

java -cp d:/work/tmp Hello--->在任意位置运行class文件

### 3.带包声明的源文件

编译跟上面一样javac -d d:/work/tmp d:/work/Hello.java

运行：

​	1.先把路径 切换到tmp中，java com.hy.Hello

​	2.任意位置：java -cp d:/work/tmp com.hy.Hello

java -cp d:/work/tmp com.hy.Hello hello abc----hello和abc是给main方法传入两个参数，两个参数放在String args[]数组中

## 4.基本数据类型

###4.1.进制

10进制

2进制：0b开头,0b101

8进制：0开头

16进制：0x开头


       			'a'为97，'A'为65，'0' 为48
       			char c='a';
       			char c= '\u0041';
       			c = 'A';			
  ​			 			



 转义字符：    

```
	\r:mac系统换行符
	\n:linux或unix系统换行符
	\r\n:windows中才能换行
	\\
	\'
	\b:退格
	\t:制表符
	
```

+ 浮点型:科学计数法存储，比如31456789,在程序中3.1456789和7
  + 单精度型:      float:4字节
  + 双精s度型:      double:8字节


### 4.3 种不同类型之间的相互运算

布尔型数据不能参与任何运算

#### byte,short,char,int类型之间的运算

当表达式中**只有**这四种时，会自动把所有类型转换为int，运算结果也为int

除此之外，所有类型之间运算时，会自动把所有数据类型转换为表达式中最大的类型

### 4.4 类型转换

int m=(int)3.14;
float f=(float)3.123456789123456;



## 5.变量的命名规则

1.只能用下划线、字母（52）、美元符开头

2.同上，并加上数字

3.不能用java中的关键字作为变量名,

比如：int 3a,,#id,*a,_b,my,java

## 6.局部代码块

{	

​	作用是提前回收不再使用的栈内存

}

## 7.运算符

### 1.算术运算符

+,-,*,

+ /:当左右两边都是整数时，为取整数运算

当左右两边只要有一边是浮点数时就是除法

+ %:取余数:运算结果的符号只跟左边保持一致，跟右边符号无关


+ ++,--:
  + 如果在右边，先缓存之前的值，然后再加或减一，最后用缓存的值参与运算
  + 如果在左边，然后用加或减一之后的值参与运算
  + ++和--的优先级高+和-




+=,-=,*=...,注意：右等号右边比较复杂时，把右边作为一个整体加小括号来处理



### 2.关系运算符

```
>,<,>=,<=,==,!=
```

### 3.逻辑运算符

+ &&,||,!


+ 短路现象：当&&的左边为false时，会发生短路，当||的左边为true时，也会短路


+ 运算的优先级

  .   >   ()   >   !  >  算术  >  关系  >  逻辑

练习

```
1.设x和y均为int型变量，且x=1,y=3,求1+x/y的值.

2.求6*7%2+7%2*5的值

3.int i=6,j,j=(++i)+(i++),则j的值为多少

4.int x=12,n=5;x%=(n%=2);求x的值

5.已知字母a的ascii码为十进制数97，且设ch为字符型变量，则ch='a'+'8'-'3'的值为多少？

6.表示“整数x的绝对值大于5”时值为“真”的java语言表达式

7.int x = 100; x++;  x+++x-10-x是多少？
```

switch(常量表达式){	

case xx:

break;

....

default:

​	xxx

}

switch小括号中可以是char,String,枚举，Integer,byte,short,int



练习

```
2.
所谓反序数就是一个数字的顺序颠倒过来，比如102的反序数是201,12的反序数是21，反序数唯一的要求就是不可有以0结尾的数，现在有个三位数与它的反序数乘积是280021，用java求出这个数与其反序数*/
public class Test {
	public static void main(String[] args) {
		int tmp = 0;
		for(int i = 101;i<=500;i++){
			if(i%10==0){
				continue;
			}
			
			int a1 = (i%10)*100;
			int a2 = ((i/10)%10)*10;
			int a3 = i/100;
			tmp = a1+a2+a3;

			if(tmp*i==280021){
				System.out.println(i+":"+tmp);
			}
		}
	}
}
```

## 9.循环语句

+ for(Object o:arr){

  }



## 10.continue,break,return区别

continue:提前结束本轮循环，并开始下一轮循环

break:提前结束当前循环或switch语句

return:提前结束当前函数的运行

System.exit(0):关闭虚拟机

# 二、数组

## 1.局部变量

+ 局部变量的内存：局部变量所占用的内存必须是在栈中申请

## 1.一维数组

数组变量所占用的内存用来存放数组的地址编号

```
数组的定义：
1.int []arr = {1,2,3,4};
	//arr = {1,2,3,4};错误，因为直接用大括号的方式只能应用在定义数组变量并初始化时可以使用
2.int[]arr = new int[4];//这种定义方式，数组元素的默认值为0或false,或null
3.int[]arr = new int[]{1,2,3,4};
```

## 2.二维数组

```
int[][]arr = new int[2][3];//两个元素，每个元素都是数组的地址编号
arr = new int[2][];//arr数组中两个元素，每个元素的值为null

int[][]a1=new int[3][];
a1[0]=new int[3];
a1[1]=new int[]{4,5,6};
a1[2]=new int[]{7,8};
```


练习

	12.编写一函数，实现复制数组中元素的功能，要求能复制指定元素个数的功能
	public static void copyArr(int[]src,int[]dest,int num){
	
	}
## 3.数组排序

### 1.选择排序

每一轮循环都找出一个最小元素放在前面，排序原理：

arr[i]跟arr[i+1],arr[i+2]....等一个个的比较

### 2.冒泡排序

第一轮结束后，最大的元素放最后，且下一轮都会减少一个元素参与比较

## 4.工具类的使用

### 4.1. Arrays.sort(数组名)

只能升序排列

### 4.2 其它方法

+ Arrays.toString(arr)---用于把数字转为字符串

+ fill():用于填充数组

+ equals():比较两个数组内容是否相等


数组拷贝：

​	**Arrays.copyOf()**

​	System.arraycopy()

​		用法：arraycopy(arr, 3, arr1, 1, 2)把arr中下标为3开始拷贝元素到arr1中的下标为1位置，一共拷贝2个元素

# 三、面向对象

## 1.什么是面向对象

对现实世界运行方式的模拟，按人们思维习惯来描述问题、解决问题

## 2.什么是类、对象

​	类：是抽象的，是一种数据类型，也是一种数据封装机制和类型定义机制

​	对象：是类的一个【具体实现】

​	引用类型变量：只要数据类型不是基本数据类型，就一定是引用数据类型，引用类型的变量中存储的是对象的地址编号

​	局部变量：在函数内存定义的变量是局部变量,局部变量的内存在栈内存申请

​	全局变量：也叫属性，在类内部定义的变量叫全局变量，也叫属性

## 3.类的属性

属性在不赋值时有默认值为0，false,null

### 1.成员变量与成员方法

​	在类内部定义的变量叫成员变量，当产生对象时，成员变量才存在

​	在类内部定义的方法叫成员方法，该方法前面不能有static

​	成员变量和成员方法必须通过其自身的某一个对象来调用

​	

### 2.静态变量也叫类变量

​	在内存中只有一份，在类加载后产生，访问时通过""类名.变量名""访问

### 3.什么时候会发生NullPointerException?

当空的变量(对象)访问其本自身的成员变量或访问其成员方法时，必发生空指针异常

## 4.权限修饰符

### private

​	私有属性或私有方法只能在其所属类内部访问

​	只能修饰方法、属性

### 包级：

​	包级权限，就是没有任何权限修饰符，这样的权限只能在同一个包内访问

​	可修饰方法、属性、类

### protected:

可修饰方法、属性

权限等同于包级，同时在包外的子类**内部**也可访问

### public

​	公有权限，在当前工程的任意位置都能访问

​	可修饰方法，属性，类

​	一个.java文件中，只能有一个public修饰的类



练习：

市场上有10只狗，分别叫狗1，狗2，...狗10，被小明随机买三只,打印出被收养的三只狗的名字

Dog

​	String name;

Market

​	Dog[]arr=new Dog[10];

Person

​	String name;

​	Market market;

​	Dog[]d = new Dog[3]



​	void getDog(){

​	}

 ![小明买狗](picture\小明买狗.png)



## 5.构造方法

也叫构造器、构造函数，constructor

特点：名字跟类名一致，没有返回值类型

作用：当new 一个对象时，一定会自动调用某一个构造方法

如果一个类没有定义任何构造方法，则会自动产生一个无参的构造方法

如果自定义了构造方法，则不会自动产生无参的构造方法

## 6.this关键字

+ this.成员变量名或成员方法名

用于访问成员变量或成员方法，用在类内部，当方法内部出现跟成员变量重名的局部变量时，默认使用的是局部变量，加上this.可表示成员变量

+ this()

用于调用本类中其他构造方法，只在构造方法内部的第一行使用

## 7.三大特征之封装

封装、继承、多态为面向对象三大特征

将属性私有化，并为每个属性提供一个用于获取其值的get方法和赋值的set方法，这种作法叫封装

作用：由于隐藏了其内部信息的细节，使内部信息不易受到破坏，同时为外界提供了访问途径，防止无效数据出现

set命名规则：没有返回值，名字setXxx，参数为属性的类型

get命名规则：返回值类型跟属性一致，名字getXxx,没有参数 ，对于boolean型属性，其名字一般为isXxx

## 8.变量的种类

### 1.从作用域中分：

+ 全局变量：在类内部定义的变量为全局变量
  + 成员变量

  + 类变量：静态变量，静态属性


+ 局部变量:在方法内或代码块内部定义的变量为局部变量


### 2.从变量的类型上分：

​	引用类型和基本数据类型

## 9.static 

### 1.static修饰属性：

该属性叫静态变量、类变量、静态属性

​	访问最好通过类名访问

###2.static修饰方法:

该方法叫静态方法、类方法

​	静态方法内部不能**直接**访问成员变量，也不能直接访问成员方法

​	成员方法内部能访问静态变量、也能访问静态方法

### 3. static修饰代码块

静态代码块

练习

```
1.定义一个类，要求能构实现查询该类一共创建了多少个实例对象？
```

## 10.代码块

### 1.局部代码块

​	在**方法**内部写一对{}，如

```
//局部代码块:用于提前释放不使用的栈内存
{
	int a = 20;
}
```

### 2.初始化块

​	在**类**的内部写一对{},

```
//初始化块:用于初始化对象的相关内容,在产生对象后会自动执行,在执行构造方法之前执行
{
    age = 20;
    name = "hello";
}
```

### 3.静态代码

在初始化块上加static

```
//在类加载完毕后就执行,只会执行一次
static{
    country = "中国";
    //age = 10;错误
}
```

## 11.对象产生的过程

1. 读取class文件产生类对象，这一过程叫类加载
2. 给静态变量赋初始值
3. 执行静态代码块
4. 产生对象，给成员变量初始值
5. 执行初始化块
6. 执行构造方法

练习：定义一个类，要求可以查询该类一共创建了多少个实例对象


	public class Person {
	  static int count;
	  {
	      count++;
	  }
	}

​	Math.pow(x,y)---x的y次方

## 12.包的使用

1. 包的定义规范

   package com.baidu.tv.news;

2. 导包

   import java.util.*;

   import java.util.Scanner;

   import static java.util.Arrays.*;--直接导入类Arrays中的方法，在程序中使用时不用写类名，直接使用其中的方法

   当多个包下有同名类时，为避免冲突，可在使用时，直接用包名加类名的方式,如:java.sql.Date

## 13.继承

### 1.概念

以已经存在的类为基础建立新类的一种技术

### 2.特点

新创建的类可以增加新的属性和功能，也可以访问父类的数据和功能

### 3.优点

1. 可使代码简洁
2. 便于维护
3. 可创建更为特殊的类型

### 4.重写

1. 概念：

   也叫覆盖，override,在子类中重新定义父类中已经存在的方法，这种行为叫重写

2. 特点：

   + 方法名和参数列表(个数，类型，顺序)和返回值类型跟父类相同
   + 重写的方法的权限要大于或等于父类权限
   + 子类方法中所抛出的异常种类只能是父类的一个子集

### 5.重载

1. 概念

   在一个类中，定义多个同名方法，但方法的参数列表不相同，这种行为叫重载overload

2. 特点：

   + 方法名相同
   + 参数列表不同（个数、类型不同，位置）
   + 跟权限、返回值类型**无关**

3. 重载与重写区别：

   1.overload:同一个类中，方法名相同，参数列表不同

   2.override:父子类中，方法名相同，参数列表相同，返回值相同

### 6.super与this

+ this()用来调用当前类的构造方法，super()用来调用父类的构造方法，这两种用法都必须放在构造方法的第一行
+ this.用来访问当前类的成员方法或成员变量,super.用于子类内部，用来访问父类中的成员方法或成员变量

## 14. 多态

### 1.概念

引用类型变量指向其子类的对象为前提，该变量在访问方法时，访问的是该变量所指向的**实际类型**对象中的方法

多态也叫动态绑定，或运行时绑定，其原理 是：当虚拟机调用 一个实例 **方法**时，它会基于对象实际类型（只能在运行时获取实际类型）来选择所调用的方法

注意：

​	1.属性没有多态性

​	2.多态的变量不能访问子类**特有**的方法和属性

### 2.类型转换

+ 自动转换

  1.对于基本类型，可以用大的数据类型变量指向小数据类型的变量或值，boolean例外

  double d = 5;

  int a = 5;

  double c = a;

  2.对于引用类型，可将子类的对象赋值给父类的实例 ，这种作法叫向上转型或向上造型

+ 强制转换

  1.对于基本类型，可用()方式把一个大类型的变量或值强制赋值给小类型变量,如

  ```
  int a = (int)3.14;
  double d = 2.2F;
  a = (int)d;
  ```

  2.对于引用类型，将父类实例强制转换为子类实例，称为向下转型或向下造型

  ​	当强制类型转换时，可以把一个变量转换为**其实际类型本身或其父类系列**

### 3.instanceof关键字

判断左边变量所指向的实际类型是否为右边类型的对象或右边类型子类系列的对象



作业：

Grade

​	String name;

​	一个参数的构造方法

Stu

​	String name;

​	Grade grade;

​	Stu[]friends;

​	Stu(String name){}

​	Stu(String name,Grade g){

​	在该方法中要求输入当前对象的朋友数量

​	初始化他的每个朋友，要求调用一个参数的构造方法,要求指定每个朋友的班级和姓名

​	}

​	void show(){

​	查看该学生的每个朋友信息

​	}

## 15.可变参数

在方法的参数列表中，可使用:   类型... 形参名这种方式，在方法内部当作数组来处理，在调用时，可传入任意多个参数,但要注意：可变参数在一个方法中只能有一个且必须放在参数列表的最后

在可变参数发生重载时，可变参数的方法优先级低

```
void sum(int x,int y,int ...a){
	System.out.println("三个参数的可变参数");
}
```



## 16.值传递与引用传递

​	在调用方法时，如果参数类型为基本数据类型，则传递的参数为数值本身，如果参数类型为引用类型，则传递的参数为数据的地址编号

## 17.final关键字

### 1.修饰局部变量

局部变量只有一次赋值机会，被final修饰的形参只读

### 2.修饰成员变量

可在定义属性时初始化、初始化块中、构造方法中初始化，有且必须仅有一次机会被初始化

### 3.修饰静态变量

可在定义属性时初始化、静态代码块中初始化，有且必须仅有一次机会被初始化

### 4.修饰类

​	该 类不能被继承

### 5.修饰方法

​	该方法不能被子类**重写**

# 四、常用类

## 1.字符串String

+ length():返回字符串中字符的个数

+ charAt(int index):返回字符串中下标为index的字符

+ compareTo(String other):大于0表示当前字符串大于参数，小于0表示当前字符串小于参数，等于0表示两个字符串相同

+ contains(String sub):判断是否包含sub 这个字符串，返回boolean值

+ 静态方法copyValueOf(arr):可把字符数组转换为字符串(不掌握)

+ toCharArray():把字符串转换为字符数组

+ 编码与解码
  + 编码：把字符串转换为字节数组，称为字符串的编码,用方法getBytes("gbk或utf8")
  + 解码：把字节数组按指定的格式转换为字符串，这种格式叫解码,new String(byte[]arr,"gbk或utf8")

+ strartsWith(String sub)和endsWith(String sub):判断字符串是否以指定小字符串开头或结尾

+ equals:比较两个字符串内容是否相同

+ split():字符串拆分，按指定的分隔符来拆分，返回的是字符串数组

+ indexOf(String sub):查询参数在字符串中出现的位置,如果不存在就返回-1，lastIndexOf(参数)：从最右侧查找

  indexOf(String sub,int index):从指定位置开始查找字符串

+ isEmpty():判断字符串是否为空字符串

+ replace（String old,String newStr):把字符串中所有出现old的内容都替换成newStr

+ subString:字符串截取

  示例：s.subString(2): 从下标为2开始截取到尾部,subString(2,4):截取的元素范围是[2,4)

+ toLowerCase和toUpperCase():转换成全小写或全大 写

+ trim():过滤掉字符串前和后的空格,不过滤中间空格

+ 字符串常量：

  + 什么叫字符串常量：直接以""包括的字符串为字符串常量，String s="abc";
  + 目的：提高性能和减少内存开销，实现数据共享。
  + 存放在哪里？**字符串字面量没有放入运行时常量池**，而是**在堆中创建了对象**，**再将引用驻留到字符串常量池中**
  + 什么时候使用字符串常量：当一个字符串要经常使用时，就用常量

  intern()：会从字符串常量池中查询当前字符串是否存在，若不存在就将当前字符串放入常量池中，若存在就返回常量池中地址。

+ 正则表达式

  ```
  [abc]:a或b或c
  [^abc]:任意非a,b,c
  [a-z]或[A-Z]或[a-zA-Z]:字符区间
  [0-9]或\d:纯数字，\D:表示非数字,[^\d]
  .:任意字符
  ^:表示匹配字符串的开始部分，一个字符串的开始标识，如果出现在[]内部则表示非
  $:表示匹配字符串的结束位置
  \w:表示匹配数字、字母、下划线,\W:跟\w相反
  \s:匹配任意空白字符(空格和tab)
  
  X?:匹配要小于等于一次，如[s]?表示字符串必须是s或空字符串
  X*:0或任意次，如[s]*表示为空字符串，如果不为空则起始字符必须是s,后面如果还有字符，也必须是s
  X+:大于等于一次
  X{n}:不多不少刚好n次
  X{n,}:大于等于n次
  X{n,m}:大于等于n次，小于等于m次
  
  如何使用正则表达式：
  1.使用String中的方法matches(reg)
  2.f = Pattern.matches(reg, s);
  3.
  //对于一个规则，如果要多次使用，可只用编译一次
  Pattern p = Pattern.compile(regex);
  Matcher m = p.matcher(input);
  f =  m.matches();
  ```









## 2.包装类/封装类

###1.概念：

java为8种基本数据类型都提供了一种对应的类，这种类叫包装类

### 2.作用：

基本数据类型功能简单，不具有面向对象特征，所以为每个基本数据类型提供了封装类，使其具有面向对象特征，从而为基本数据类型的操作提供了工具方法

### 3.基本类型与包装类型转换

#### 1.装箱

​	将基本数据类型转换为对应的包装类型

#### 2.拆箱

​	将包装类型转换为对应的基本数据类型

### 4.有哪些包装类型？

byte--->Byte -128~127

short-->Short

int---->Integer

long-->Long

boolean-->Boolean

char--->Character  0~65535

float-->Float

double-->Double

### 5.基本类型与字符串转换

####1.基本类型转换成字符串

String s = 10+"";

s = true+"";

#### 2.字符串转基本类型

Xxx.parseXxx(String s):返回对应的基本类型，xxx表示对应的包装类型

Xxx.valueOf(String s):返回对应的包装类型,

示例：int n = Integer.valueOf("10");

n = Integer.parseInt("10");



## 3.可变字符串

### 3.1 常用方法

StringBuffer:实现了多线程的同步，所以在多线程中是安全的，但效率低

StringBuilder:没有实现多线程同步，所以在多线程中不安全，但效率高

可变字符串与不可字符串String之间的互转

new StringBuffer(String s)---把String转为可变字符串

toString()---把可变字符串转为String

常用方法：

+ append(Object data):追加内容
+ toString():把可变字符串变为String类型
+ insert(int index,Object data):在指定位置插入数据
+ delete(int begin,int end):删除指定范围的字符串
+ replace(int begin,int end,String s):把指定范围的内容替换成指定内容
+ reverse():倒置字符串



### 3.2 面试题

+ 拼接字符串的几种方法？

  String s1="abc";  String s2="123";

  + 方法一

    String s=s1+s2;

  + 方法二

    s=s1.concat(s2);

  + StringBuffer sb=new StrinbBuffer();

    s=sb.append(s1).append(s2).toString();

+ String与StringBuilder,StringBuffer在性能上区别：

  + String:不可变字符串，当要频繁编辑字符串时，String效率低
  + StringBuilder:法没有实现线程同步锁，多个线程可同时访问，所以不安全，但多线程中效率高.适用于单线程下在字符缓冲区进行大量操作。默认为16个元素的字符数组,如果容量不够用，则扩容为max(2*数组长度+2,数组长度+s.length)
  + StringBuffer:多线程中安全，但很多方法都添加了Synchronized锁，无法并发修改，在多个线程同时访问时，只能一个线程访问，所以安全,效率低，用于多线程下，在字符缓冲区进行大量操作的情况


## 4.位运算符

+ 数字在内存中的存储方式
  + 正数：以原码方式存储，如int数字2，在内存中000...00010
  + 负数：以补码方式存储，如-2，在内存中就是原码100...0010-->反码，符号位不取反：111...1101-->补码，反码+1：111...1110


+ 位逻辑运算

  + ~:按位取反：对应位上取反，包含符号位

  + &：按位与：对应位上按位与，只有全为1结果为1，否则为0

  + |:按位或：只有全为0结果为0，否则 为1

  + ^:按位异或:对应位上相同则取0，不同取1

    注意：位运算符不会短路



+ 移位运算
  + \>>:带符号位的右移，低位丢弃，高位补1或0，如果负数则补1，正数补0
  + <<:左移：整体左移，高位直接丢弃，低位补0
  + \>>>:不带符号位的右移，低位丢弃，高位补0

注意：移位运算符优先级低于算术

+ 面试题：计算2*8，如何实现提高效率？2<<3

## 5.抽象类

用abstract关键字修饰的类叫抽象类

+ 特征：
  + 可以有抽象方法,也可没有，也可以有具体方法,也可以有构造方法；
  + 抽象类不可产生对象


+ 什么是抽象方法？

  用abstract修饰的方法叫抽象方法,

+ 抽象方法有什么用？

  在发生多态时，可正常调用子类的具体方法

注意：抽象方法不能被static,final,private,native,synchronized修饰 



## 6.接口

用来描述不同类事物的共同特征的方式

###1.特征：

1. 接口的属性必须且默认是public static final型的

2. 接口中的方法通常是public abstract型方法

3. 接口中可以有具体方法，但要添加default关键字

4. 接口可以用extends继承自其他**多个**接口，接口之间用逗号隔开

5. 接口中没有构造方法,不能被产生出对象

6. 类可同时实现**多个**接口,逗号隔开

   类与接口的关系描述

   + 类与类之间：继承，且单继承

   + 类与接口之间：实现，implements,多重实现

   + 接口与接口之间：继承，多继承

### 2.接口与抽象类的区别：

* 接口可多重继承(extends)
* 接口中方法如果要具体化，必须添加default，而抽象类可以正常实现方法
* 接口中的属性是public static final型 的，抽象类不是
* 抽象类可以有构造方法，接口不能有
* 接口中的方法是public abstract或default修饰,抽象类中可以有任意类型方法


### 3.对象数组排序

数组中元素所属的类要实现接口Comparable,重写其方法public int compareTo(Object o)

升序：在方法内返回this-o; 降序：返回o-this

## 7.日期类

**Date**:

setTime(long time):通过一个long型的数值设定时间
getTime():获取毫秒数

**SimpleDateFormat**：用于实现字符串与日期之间一互转

	yyyy-MM-dd HH:mm:ss---表示年月日时分秒，hh表示12小时制，HH表示24小时制
	String format(Date d):把一个日期对象转换为指定格式的字符串
	Date d = parse(String msg):把一个字符串按照指定格式解析成一个日期

**Calendar:日历类**

	c.set(2022, 8, 20):把日历改为指定的时间
	add(日期项，val):给一日历增加指定的日期项一定的数值
	setTime(Date date):修改成一个指定时间的日历
	getTime():返回一个日期
	get(日期项):可获取当前日历的指定日期项
	getTimeInMillis():获取毫秒数
	System.currentTimeMillis():毫秒数​  

## 8.枚举

	只能产生固定的对象，在定义时已经确定好了，作为属性存在
	枚举的属性全是public static final型 属性，类型跟枚举的类型一致
	public enum Course {
		JAVA,
		TEST,
		UI;
		Course(){
			System.out.println("....");
		}
	
		public String toString() {
			return "abc";
		}
	}



## 9. 随机类和Math

+ Math:
  + random();///[0~1)
  + pow(a, b)--a的b次方
  + round(a)----四舍五入
+ Random
  + nextInt(50)---[0~50)
  + nextBoolean()---true 或false

## 10.内部类

### 10.1 内部类




	public class Outer {
	int a = 5;
	//普通内部类
	class Inner{
		int a=10;
		//内部类中只能定义final型static,不能定义static
		static final int m=10;
		void showB(){
			System.out.println(this.a);//内部类的属性
			System.out.println(Outer.this.a);//访问外部类属性
		}
	}
	
	//静态内部类
	static class In{
		//可定义普通静态变量
		static int n=100;
		void showC(){
			System.out.println(n);
		}
	}
	void f(){
		//局部内部类
		class Local{
		}
		new Local();
	}
	
	//内部接口
	interface Intf{
		
	}
	}
	public class Test {
	public static void main(String[] args) {
		Outer a = new Outer();
		//创建对象时，必须先创建外部类对象，通过外部类对象调用内部类构造
		Inner b = a.new Inner();
		b.showB();
		System.out.println(Inner.m);
		
		//产生静态内部类
		In i = new In();
		i.showC();
	}
	}




###10.2 匿名内部类

​	产生匿名内部类的前提是必须有:父类或接口

​	特征：

+ 在内部类内部如果访问外部的变量，则只可读取不能修改,外部的变量实际是final类型

+ 内部类内部不能有静态方法

+ 内部类中定义的属性在外部不能被访问

  重点掌握如何定义匿名内部类

# 五、异常

# 1.概念

### 1.什么叫异常

程序在**运行期间**发生的各种错误或非期待的问题叫异常

### 2.Throwable异常种类

####Exception:

+ 检查异常:编译器会强制要求处理,所有非运行时异常都叫检查异常

+ 非检查异常:异常问题可通过修改代码避免发生的异常叫非检查异常，也叫运行时异常

  只要是RuntimeException或其子类都是运行时异常

#### Error:

​	虚拟机或硬件原因造成的错误

##2.关键字

###1.finally

​	会执行

### 2.throws与throw

throw:手动抛出异常

​	在一个方法中，如果抛出的是检查异常，则必须处理或用throws声明抛出

throws:声明一个方法可能会抛出什么种类的异常

## 3.自定义异常


	public class PasswordException extends Exception {
	    public PasswordException() {
	        super();
	    }
	    public PasswordException(String msg) {
	        super(msg);
	    }
	    public PasswordException(String msg,Throwable e) {
	        super(msg,e);
	    }
	}

## 4.垃圾回收机制

java中的对象当没有引用变量指向它时，虚拟机会在适当时机回收这些对象

finalize方法：在对象在被虚拟机回收前一定会自动调用 其finalize方法

System.gc():提醒虚拟机回收内存

 ![gc-回收原理](picture\gc-回收原理.png)

## 5.对象克隆

对象克隆是java 	23种设计模式中的**原型模式**,对象克隆不会调用构造方法

克隆步骤：

+ 待克隆对象所属的类要实现接口Cloneable,该接口没有方法


+ 要在类内部重写Object类中的方法clone,并在方法内部要调用Object中的clone方法



# 六、IO

## 1.File类

注意：路径写法

+ /

+ \\\\

+ //

  "work/1.txt"---相对路径

  "d:/work/1.txt"--绝对路径

  "/home/a.txt"---绝对路径

### 1.构造方法：

+ File(String path)
+ File("d:/work","1.txt"):参数1：表示文件所在目录，参数2：表示文件名
+ File(File parent,String 文件名):文件名所在目录用parent文件对象表示



### 2.文件对象常用方法

+ getName():获取文件名
+ getAbsolutePath():获取文件绝对路径
+ getCanonicalPath():也获取绝对路径，但会解释"d:/work/../1.txt"为"d:/1.txt"
+ getPath():获取文件相对路径
+ getParent():获取父目录所在路径
+ getParentFile():获取父目录的文件对象
+ exists():判断文件或文件夹是否存在
+ length():文件的大小，也就是字节数
+ delete():删除文件
+ mkdir()：创建文件夹，如果该文件夹所在目录不存在，则无法创建
+ mkdirs():也是创建文件夹，如果所在的父目录全都不存在，则一起创建
+ createNewFile():创建文件
+ isFile():判断是否为文件
+ isDirectory():判断是否为文件夹
+ list():把当前目录中所有的文件夹和文件的名称放入数组中并返回String[]
+ list(FilenameFileter filter):用接口的方式 过滤
+ listFiles():列出目录中所有的文件对象，也可用接口的方式实现过滤，listFiles(FileFilter filter)

## 2、流

### 2.1 分类

```
按传输方向分为输入流和输出流
按传输的单位分为字节流和字符流
```

| 输入流      | 输出流       |        |
| ----------- | ------------ | ------ |
| InputStream | OutputStream | 字节流 |
| Reader      | Writer       | 字符流 |



###2.2文件字节流

####1).构造方法

#####a.文件字节输入流

+ FileInputStream(File f)
+ FileInputStream(String path)

#####b.文件字节输出流

+ FileOutputStream(File f)
+ FileOutputStream(File f,boolean append):是否追加内容
+ FileOutputStream(String path)
+ FileOutputStream(String path,boolean append)

注意：所有的输出流都会自动创建文件

####2).常用方法

#####a.输入流

available():评估本次在输入流不被阻塞的情况下能获取多少字节

skip(n):从当前位置跳过n个字节

11111111  11111111 11111111 11111111

int read():(重要)一次读取一个字节，并把所读取的字节数据放入返回值中,返回值为int类型,也就是说，返回值为所读取的数据,示例如下：

​	

```
int data;
while((data = fis.read())!=-1){
	System.out.print((char)data);
}
```

int read(byte[]arr):(重要)执行一次该方法，读取最多arr.length个字节，返回值表示本次读取了多少个字节,示例如下：

```
byte[]arr = new byte[3];
int len;
while((len = fis.read(arr))!=-1){
	System.out.print(new String(arr,0,len,"gbk"));
}
```

int read(byte[]arr,int off,int len):每次读取最多len字节，放到数组中下标为off位置处，返回值表示本次读取多少字节

#####b.输出流

+ void write(int data):参数表示要写入的数据，一次写一个字节(重要)
+ void write(byte[]arr):把数组中的全部内容写入文件中
+ void write(byte[]arr,int off,int len):把数组中从off位置开始，取len个元素写入文件中(重要)

#### 2.3 标准流(不掌握)

+ System.in:默认从键盘缓冲区获取数据
  + System.setIn(fis);重定向标准输入流

+ System.out:默认向控制台打印
  + System.setOut(out);重定向标准输出流到其它流中

###2.4 路径问题

#### 1.获取文件

```
//1.从当前工程中读取文件
	FileInputStream fis = new FileInputStream("work/1.txt");
//2.从当前工程的src目录中读取文件,通过类加载器获取资源时，相对路径是相对的src
	InputStream fis = Test.class.getClassLoader().getResourceAsStream("work/常用单词");
		
//3.从当前类所在目录中读取,相对的是当前类所在目录
	fis = Test.class.getResourceAsStream("a.txt");

```

#### 2.写入文件

```
//1.在当前工程根目录下的文件中写入数据
FileOutputStream fos = new FileOutputStream("1.txt");
//2.在当前工程的src目录下的文件中写入数据
String path = Test2.class.getResource("/").getPath();
fos = new FileOutputStream(path+"/1.txt");
//3.在当前类所在目录中的文件中写入数据
path = Test2.class.getResource("").getPath();
```

## 3.其它输入输出字节流

### 1.字节数组流

+ ByteArrayInputStream(byte[]arr):从一个指定的数组中读取数据
+ ByteArrayOutputStream():把数据写入到内部的数组中
  + 常用方法：toByteArray()返回内部的字节数组
  + toString():返回字符串

### 2.字节缓冲流

+ BufferedOutputStream

  ​	flush():刷新缓冲区

  ​	close():先刷新缓冲区，再关闭所包装的流

+ BufferedInputStream

### 3.面试题

> 定义一个数组，如int[]arr={12,24,18,20},把数组里的数据写入某个文件中，并读取出来打印到控制台，要求按倒序方式打印输出，如输出结果为{20,18,24,12}

## 4.字符流

### 1.常用方法

write(int data):每次从int参数中取两个字节写入输出流中

write(char[]arr):把字符数组中内容写入输出流中

write(char[]arr,0,len):把字符数组中一部分内容写入输出流中

write(String s):把字符串全写入输出流中

write(String s,0,len):把字符串的一部分写入输出流中



int read():每次执行读取一个字符，返回值表示本次读取的数据

int read(char[]arr):每次都把数据读取到数组中，返回值表示本次读取了多少个字符

###2.常用字符流

+ FileWriter
+ FileReader
+ BufferedWriter
  + 方法：newLine():向输出流中写入一个换行符
+ BufferedReader
  + 方法：readLine():每次读取一行,但不包括换行符
+ PrintWriter
  + println和print
+ PrintStream:
  + 方法：println和print
+ CharArrayWriter
+ CharArrayReader

## 5.随机流RandomAccessFile 

参数1表示要处理的文件

参数2表示要进行什么种类的操作：r--只读,rw--可读可写，

常用方法：

+ seek(n):跳转到文件某个位置，参照点为0
+ skipBytes(n):从当前位置向右跳转n个字节
+ write(int a):写一个字节
+ writeBoolean(boolean b):写入一boolean 值
+ writeUTF（）和readUTF():用来读写字符串

## 6.转换流

​	把字节流转换为字符流

### 1.InputStreamReader

### 2.OutputStreamWriter

## 7.对象序列化

+ 概念

  + 序列化：把内存中的对象转换为字节写入文件或通过网络传输到远端服务器
  + 反序列化：从文件或网络传输过来的字节数据转换为内存中的对象

+ 如何实现

  	ObjectInputStream
  	
  		readObject():反序列化一个对象，也就是读取一个对象
  	
  	ObjectOutputStream
  	
  		writeObject(o):把对象序列化

注意：

+ 对于静态属性和transient修饰的属性，在序列化时，默认无法保存
+ 被序列化的对象所属类型要实现接口SerialLizable，并添加属性serialVersionUID,值任意
+ 对于被序列化的类，可添加writeObject和readObject方法，来实现私人定制(不掌握)
+ 在反序列化时，不会调用其自身构造，如果父类没有实现Serializable接口，则会调用父类构造（不掌握）

## 8.Lambda表达式

### 1. 是什么

是对象，非函数，是函数式接口的对象

#### 1.1 什么是函数式接口？

一个接口中只有一个抽象方法，这样的接口叫函数式接口

Comparable,Runnable

#### 1.2 什么时候使用

只要可以使用匿名内部类的都可用lambda表达式替代

### 2.如何使用

####2.1 一个参数

```
public static void main(String[] args) {
		Comparable<String>c = new Comparable<String>(){
			public int compareTo(String o) {
				System.out.println("匿名内部类");
				return 0;
			}			
		};
		
		c.compareTo("abc");
		System.out.println("------------------------");
		Comparable<String>d = (String o)->{
			System.out.println("lambd表达式");
			return 0;
		};
		Comparable<String>e = o->{
			System.out.println("lambd表达式");
			return 0;
		};
		
		e.compareTo("abc");
	}
```

#### 2.1 多参数

```
Comparator<Integer> c = new Comparator<Integer>(){
			public int compare(Integer o1, Integer o2) {	
				return o1.compareTo(o2);
			}
		};
		System.out.println(c.compare(100, 20));
		
		
		c = (o1,o2)->{
			return o1.compareTo(o2);
		};
		c = (o1,o2)->o1.compareTo(o2);
```

### 3.方法引用

#### 3.1 四大函数式接口

+ Consumer<T>---void accept(T t);消费型接口
+ Supplier<T>---T get();供给型接口
+ Function<T,R>--R apply(T t)，函数式接口
+ Predicate<T>---boolean test(T t),判断型接口

#### 3.2 方法引用的三种情况

要求接口中的方法跟被引用的方法参数类型一致，返回值类型一致

+ 对象::对象的某个方法

  ```
  Consumer<String>c1=ps::println;
  c1.accept("hello,world");
  ```

+ 类::静态方法

  ```
  Comparator<Integer>c1 = (n1,n2)->Integer.compare(n1, n2);
  c1 = Integer::compare;
  System.out.println(c1.compare(100, 200));
  ```

+ 类::成员方法

  ```
  Comparator<String>c1 = (s1,s2)->s1.compareTo(s2);
  c1 = String::compareTo;
  System.out.println(c1.compare("hello", "abc"));
  ```


## 9.ThreadLocal和volatile

### a.threadLocal变量：

1. 作用：为每个线程都创建一个副本，每个线程在使用变量时，都只能使用自己的变量
2. 原理：原理：看set方法的原代码。每个线程内部都有一个自己的map,是通过Thread.currentThread获得自己的map,通过m对象set时，每个线程都使用自己的map,key为m本身
3. remove方法：向local中添加了数据a后，如果把a=null,依然无法回收a对象，**导致内存泄漏**，使用remove可回收。
4. 经典应用：把数据库的连接对象con放到local中



###b. volatile变量：


+ 应用背景

java内存模型规定所有的变量都是存在线程共享内存(主内存)中，每个线程都有自己的线程独占内存（本地内存）。线程对变量的所有操作都必须在线程独占内存中进行，而不能直接对共享存操作。每个线程也不能访问其他线程的独占内存。变量的值何时从线程的独占内存写回共享主存，无法确定。



+ ![](picture\线程-volatile.png)

+ 作用：被volatile关键字修饰的变量，如果值发生了变更，其他线程立马可见
+ 原理：
  + 在写时，直接到主内存写
  + 在读取时，直接到主内存中读取数据


+ 虽然可保证变量的修改对其他线程可见，但不能保证修改的原子性(如果多个线程同时修改，仍然会有安全问题)



# 七.线程

## 1. 概述

### 1.1 进程与线程区别：

​	进程：是计算机内部运行的某个程序。

​	线程：是正在运行的某个进程内部的某个任务，这些任务可以同时（并发）执行，这些多线程可共享该进程的内存。

### 1.2 为什么要使用多线程？ ![线程-什么什么使用多线程](picture\线程-什么什么使用多线程.png)

### 1.3 线程原理

 ![线程-原理图](picture\线程-原理图.png)

## 2.线程的启动

### 1.继承自Thread

### 2.实现接口Runnable

```
A a1 = new A(1000);类A实现接口Runnable
Thread t1 = new Thread(a1);
t1.start();
```

这些方式，跟第一种对比：

+ 可以继承其他类，避免单继承的局限性
+ 线程执行的代码放在Runnable接口的实现类的run方法中，而不是放在子类的run方法中

###3. Callable接口的实现

####3.1 特点：

+ 在方法体call中可抛出异常
+ 方法体有返回值,用于在线程执行完毕后，返回执行的结果

#### 3.2 实现步骤

+ 定义类B 实现接口Callable，并实现其方法call
+ 定义对象FutureTask rs = new FutureTask(b);
+ new Thread(rs,"线程名").start();
+ rs.get():可获取执行的结果

### 4.线程池--重点

#### 4.1 背景及优点

1. 应用背景：对于一些单个任务处理时间短但任务数量大的应用，创建和销毁线程所消耗的资源比线程执行所占用资源更多，这种情况都可通过线程池解决。

2. 优点：

   > a. 重用存在的线程，减少对象创建、消亡的开销，性能佳。
   > b. 可控制最大并发线程数,从而避免占用过多系统资源导致oom

####4.2 工作原理

当请求过来时，线程已经被线程池提前创建好了，可直接申请使用。当请求结束时，线程不再销毁而是重新放回池子中再复用。当并发请求过多时，可让请求等待在队列中，甚至拒绝新请求从而避免请求太多导致系统崩溃。

#### 4.3 各种线程池特点：

+ newFixedThreadPool(n)：定长线程池

  + 特点：

    1. 定长，多出的请求放LinkedBlockingQueue队列

    2. 池子中最多n个线程，当有请求过来时，就创建线程，最多n个，第n+1个及以后的请求

       入队列中等待，直到前面线程结束，才会从队列中获取任务并启动任务

  + 适用场景：可用于Web服务瞬时削峰，但需注意长时间持续高峰情况造成的队列阻塞（队列满了）

  + 缺点：

    1. 当没有任务时，不会销毁之前创建的线程
    2. 当有大量请求都入队列时，容易导致OOM(out of memery,内存溢出)



+ newCachedThreadPool:缓冲线程池
  + 特点：使用SynchronousQueue队列，最大线程无限制，无任务时，线程存活的最大时间为1分钟
  + 适用场景：快速处理大量耗时较短的任务
  + 缺点：过多请求会使系统崩溃。


+ newSingleThreadExecutor:单一线程池
  + 特点：只有一个线程提供服务,过多的任务入LinkedBlockingQueue队列,保证所有任务的执行顺序按照任务的提交顺序执行。
  + 适用场景：所有任务的执行需要按照任务的提交顺序执行。
  + 缺点：不能处理高并发



+ newScheduledThreadPool：定时线程池

  + 特点：可重复执行或延迟执行任务。使用DelayedWorkQueue队列

  + 适用场景：需要重复的执行某个任务的场景。


    	//参数为池子中最少线程数
    	ScheduledExecutorService pool=Executors.newScheduledThreadPool(1);
    	//1秒后，每间隔3秒执行一次
    	pool.scheduleAtFixedRate(new B(), 1, 3, TimeUnit.SECONDS);
    	//三秒后执行一次
    	pool.schedule(new B(),3, TimeUnit.SECONDS);




+ ThreadPoolExecutor：自定义线程池，重点

  + 特点：自定义队列、拒绝策略、回收线程的方式、最小和最大线程数

  + 参数说明：

    + 参数1：线程池所维护线程的最小数(核心线程数)，作用是：当来任务时，如果当前线程数小于2，则立刻创建新线程，如果等于2则把任务放入队列中等待。

      BlockingQueue：是双缓冲队列。BlockingQueue内部使用两条队列，允许两个线程同时向队列一个存储，一个取出操作。在保证并发安全的同时，提高了队列的存取效率。 常用的几种BlockingQueue：

      - ArrayBlockingQueue（int i）:规定大小的BlockingQueue，其构造必须指定大小。其所含的对象是FIFO顺序排序的。
      - LinkedBlockingQueue（）或者（int i）:大小不固定的BlockingQueue，若其构造时指定大小，生成的BlockingQueue有大小限制，不指定大小，其大小有Integer.MAX_VALUE来决定。其所含的对象是FIFO顺序排序的。
      - PriorityBlockingQueue（）或者（int i）:类似于LinkedBlockingQueue，但是其所含对象的排序不是FIFO，而是依据对象的自然顺序或者构造函数的Comparator决定。
      - SynchronizedQueue（）:特殊的BlockingQueue，对其的操作必须是放和取交替完成

    + 参数2：池中线程最大数量，当线程数已经是n>=2且n<4,且队列已满，则创建新线程。

    + 参数3和4：对于那些非核心线程，如果任务执行完后线程空闲时间超过3秒，则自动销毁线程。

    + 参数5：池子所使用的消息队列。

    + 参数6：是拒绝策略，如果三者都满，则会选择拒绝未被执行的任务

      1. Discard:丢弃新请求。
      2. DiscardOldestPolicy:会把最先入队且未被执行的任务从队列删除，让新请求入队。
      3. Abort:拒绝新请求，但会抛出"拒绝执行异常"。
      4. CallerRuns:新请求会一直重复添加，直到能添加进行为止，不会漏掉任何请求。

  + 示例：

  ```
  ThreadPoolExecutor pool = new ThreadPoolExecutor(2,4,3,TimeUnit.SECONDS,new ArrayBlockingQueue(3),new ThreadPoolExecutor.DiscardOldestPolicy());
  ```


#### 4.4 池子的方法

submit(o):向池子申请线程服务

shutdown()：会把当前正在执行的线程和队列中的线程执行完毕再关闭池子

shutdownNow():会把正在执行的线程执行完毕就关闭，不再执行队列中的任务

### 5. 后台线程

守护线程，指的是程序运行时在后台提供的一种通用服务的线程。比如垃圾回收线程就是一个很称职的守护者，并且这种线程并不属于程序中不可或缺的部分。因此，当所有的非守护线程结束时，程序也就终止了，同时会杀死进程中的所有守护线程。反过来说，只要任何非守护线程还在运行，后台线程就不会终止。



## 3.常用方法

+ Thread.sleep(n):休眠n毫秒

+ Thread.currentThread():获取当前线程对象

+ start()：启动线程

+ interrupt():会让阻塞状态下的线程，抛出异常，从而实现停止线程的目的。

+ 让线程暂停的方法有：sleep(),yield(),wait();synchronized,stop()

+ 如果要停止线程，官方建议使用volatile关键来控制

+ setName()和getName（）设置或获取线程名

+ yield():让线程进入runnable，也就是准备就绪状态

+ join():会让当前线程处于阻塞，直到插入线程执行完毕才解除阻塞

+ CountDownLatch:减法器

  + await:当某个线程调用该方法时，会让该线程阻塞，直到计数器为0才解除阻塞
  + countDown():某线程调用该方法，并不会阻塞，只是把计数器减一，如果计数器为0则唤醒被awati()阻塞的线程

+ CyclicBarrier：加法器

  + await():计数加1，且阻塞，当计数为指定值时，执行指定线程（项楚亡秦）-->解除阻塞

+ setDaemon(true):把线程设置为后台线程，当没有前台线程存活时，该后台线程会自动结束。

    



## 4.线程同步

### 4.1 代码块同步

+ synchronized实现


  	synchronized(锁对象){
  		同步内容
  	}

+ Lock实现

  synchronized存在的问题：线程由于被阻塞，此时不会释放锁，其他线程如果仅仅时打算进行读操作，但该锁的存在导致无法读取。

  ```
  Lock lock = new ReentrantLock();---产生锁，默认非公平锁，如果添加参数true则是公平锁
  lock.lock();--给代码手动上锁,如果没有获得锁，则阻塞
  lock.tryLock():如果成功获取锁则返回true,没有获得锁并不会阻塞，而是立刻返回false，如果有参数，则获取不到锁就阻塞最多指定的时间。
  lock.unlock();---在finally中给代码解锁
  ```


### 4.2 方法同步

+ 成员方法的同步：锁对象是this
+ 静态方法同步:锁对象是当前类的类对象

注意：不管什么同步，多个线程之间必须共享同一个锁对象



### 4.3 读写锁

synchronized和lock方式一旦上锁，则多个线程并发读时，只能有一个线程读，不合理

ReadWriteLock loc = new ReentrantReadWriteLock();

loc.readLock().lock();---上读锁

loc.readLock().unlock();--解读锁

loc.writeLock().lock();--上写锁

loc.writeLock().unlock();--解写锁



特点：

+ 如果一个线程占有了读锁，则其他线程申请写锁时会一直等待到读锁的释放，只能读，不能写
+ 一个线程占用了写锁，则其他线程在申请读或写锁时都等待释放写锁，不能读和写

###4.4 Lock和synchronized区别

+ Lock:是接口，而synchronized是关键字
+ Lock需要手动上锁和解锁，而synchronized则自动上锁和解锁
+ Lock在并发读数据时效率高

### 4.5 信号量

+ 作用：用于限制可以访问资源的线程数目
+ 用法：
  + Semaphore sem = new Semaphore(3);---产生一个容纳3个并发的信号量对象
  + sem.acquire()---申请一个信号值，每成功申请到一个值，就会自动减一，当为0时申请不到就阻塞
  + sem.release();--释放一个信号值，就是给信号量加1



###4.6悲观锁与乐观锁

+ 悲观锁：总是假定最坏情况，每次数据被访问时，都认为别人要修改，所以其它线程在访问时会阻塞，直到拿到锁才解除阻塞，synchronized就是悲观锁，lock()也是悲观锁。并发时，写多读少用悲观锁

+ 乐观锁：每次其它线程访问时，都认为不会修改数据，所以不上锁，但是在更新时会判断一下在次期间是否被其他线程修改过,如果没有被修改过，则直接覆盖，如果被修改过则再读再写。并发时，读多写少用乐观锁

  java中的CAS技术就是乐观锁技术

  AtomicInteger：在高并发时修改不会出问题

  ​	addAndGet(n)---加n操作,n为负数就是减法，如-1就是每执行一次减1

  ​	decrementAndGet()--减1操作

  ​	get()---返回数据本身

+ CAS缺点:

  + 循环时间长导致开销大

  + ABA问题



## 5.线程间通信

### 5.1 wait()

+ 调用wait方法的代码必须放在同步代码中,且必须由锁对象来调用wait
+ wait会使线程处于阻塞状态,wait(10)最多阻塞10毫秒
+ 在wait状态下，如果被请求中断，会抛出异常
+ wait会释放锁

### 5.2 notify()

+ notify()必须出现在同步代码中,且必须由锁对象来调用notify
+ notify()会唤醒某一个通该对象发出的wait()线程
+ notifyAll():会唤醒所有通过该对象发出的wait
+ notify不会释放锁

### 5.3 Lock实现的线程通信

解决ABCABCABC...问题

通过Lock对象的newCondition（）方法产生Condition对象c,

c.awati()会让线程阻塞，c.signal()会让线程唤醒




## 6.单例

一个类只能最多产生一个对象，这种类叫单例

+ 饿汉模式：

  + 缺点：性能差，因为类加载就产生对象。

  + 优点：简单，线程安全

    ```
    public class S1 {
    	private static final S1 instance=new S1();
    	private S1(){
    		
    	}
    	public static S1 getInstance(){
    		return instance;
    	}
    }
    ```

+ 饱汉模式

  ```
  public class S2 {
  	private static S2 instance;
  	private S2(){
  		
  	}
  	
  	public static S2 getInstance(){
  		if(instance==null){
  			instance=new S2();
  		}
  		return instance;
  	}
  }
  ```

  + 缺点：不安全，因为可能产生多个实例
  + 优点：性能好，简单


+ 懒汉模式

  ```
  //懒汉模式
  public class S3 {
  	private static S3 instance;
  	private S3(){
  		
  	}
  	public static synchronized S3 getInstance(){
  		if(instance==null){
  			instance=new S3();
  		}
  		return instance;
  	}
  }
  
  下面是改进
  //懒汉模式
  public class S3 {
  	private static S3 instance;
  	private S3(){
  	}
  	public static S3 getInstance(){	
  		if(instance==null){
  			synchronized (S3.class) {
  				if(instance==null){
  					instance=new S3();
  				}
  			}
  		}
  		return instance;
  	}
  }
  ```

  + 优点：线程安全，效率高
  + 缺点：可读性 不好。





# 八、集合

## 1.结构

Collection

+ List:集合中元素可重复
  + ArrayList:顺序存储：
    + 无参构造方法：内部数组个数为0，如果带参数，则产生新数组，长度为指定参数大小
    + 扩容：
      + 当集合为无参构造创建时，第一次添加元素，扩容成10，以后每次扩容为旧容量的1.5倍。
      + 当集合为有参构造创建时，一开始创建一个指定值大小的数组，当容量不够时，扩容为当前容量的1.5倍
  + Vector:顺序存储
  + LinkedList:双向链式存储
  + CopyOnWriteArrayList：高并发下，效率高，数据安全
+ Set:集合中元素不可重复
  + HashSet
  + TreeSet
  + CopyOnWriteArraySet：可保证高并发下，效率高，数据安全
  + LinkedHashSet:可保证添加的顺序跟遍历的顺序一致

Map

+ HashMap
+ Hashtable
+ Properties
+ ConcurrentHashMap

## 2.List实现类

### 1.ArrayList和Vector的常用方法

add(Object o):在集合尾部添加元素

add(int index,Object o):在指定位置添加元素

size();返回元素个数

get(int index):获取指定位置元素

subList(from,to):截取集合的一段，返回新集合

addAll(Collection c):把集合中元素全都添加到集合中

set(index,Object o):把指定位置元素修改成指定元素

remove(index):删除指定位置元素

remove(Object o):删除指定的对象

isEmpty():判断集合是否为空

contains(Object o):判断 是否包含指定元素

indexOf(object o):获取指定元素的下标

toArray():把集合转换成数组

Arrays.asList(1,2,3)：注意，该方法返回的ArrayList是Arrays的内部类ArrayList

String.join（",",list）:把集合中元素用指定的分隔符拼接成字符串，但集合中元素必须是字符串才能用。:**JDK8新特性**

iterator():枚举器

​	Iterator有三个方法：

​	hasNext():判断当前位置是否有元素

​	next():1.取出当前位置元素;2.把当前位置向后移动

​	remove():删除当前位置元素

Collections工具类方法：

对集合排序用Collections.sort()

max和min:求集合中最大值、最小值

### 2 .vector与ArrayList区别

vector:实现了多线程同步，多线程效率低，可采用Enumeration方式遍历

​	只有Vector/Hashtable/Properties可使用Enumeration方式

ArrayList：没实现多线程同步，多线程效率高

### 3.泛型

泛型有类或接口泛型，也有方法泛型

特点：

1. 一个泛型如果不被赋值，默认为Object类型
2. 基本类型无法作为泛型赋值
3. 任何指定了泛型的对象与不指定泛型的对象之间，可相互赋值
4. 两个对象都指定了泛型，如果类型不一致，则无法直接赋值

### 4.LinkedList

特点：链式存储，但vector和ArrayList是顺序存储

常用方法：

+ addFirst:头部添加
+ addLast()等同于add
+ removeFirst():删除头部元素,remove()也是删头部
+ removeLast():删除尾部元素,
+ getFirst():获取头部元素
+ getLast():获取最后一个

## 3.Set接口的两个类

### 1.HashSet:

#### 1.1 原理


采用hash(哈唏)算法原理，实现元素的存储和查询



#### 1.2 常用方法

+ size():
+ isEmpty()
+ contains(Object o):判断是否包含指定元素
+ revmove(Object o):删除指定元素

### 2.TreeSet

采用Comparable接口来实现元素的存储，可保证元素在存储时的有序性

### 3.List与Set区别

+ List	元素存储顺序与取出顺序一致，而Set则不一致
+ List的实现类中元素都有下标，Set则不可以
+ List元素可重复，Set则不可以

### 4  写时复制

#### 4.1 原理

向容器添加元素时，不直接向数组中添加，而是先产生一备份，向备份数组中添加元素，当添加完成后再用备份数组替换当前数组

#### 4.2 集合类型

- CopyOnWriteArrayList--内部是一数组
- CopyOnWriteArraySet--内部使用CopyOnWriteArrayList，在添加元素时，判断元素是否已在数组中存在，存在就不添加

#### 4.3 应用场景

读多写少

#### 4.4 特点

**读取不加锁，写入加锁但不阻塞读**





## 4.Collections工具类

方法：

- Sort(list):升序排列

- max(list):最大值

- min(list):最小值

- reverse():倒置

  Collections与Collection区别：

  Collection:是一个集合接口，提供了对集合对象进行基本操作的抽象方法

  Collections:包含有各种有关集合操作的静态方法，不能产生对象

## 5.Map接口

Map接口的实现类中，key的存储都是采用Hash算法原理存储

### 1.HashMap、Hashtable

#### 1.1 常用方法

+    put(key,value):添加键值对

+    containsKey(k):是否包含指定的key

+    containsValue(v):是否包含指定的value

+    get(key):通过key获取对应的value

+    keySet():获取全部key的集合

+    values():获取全部value的集合

+    entrySet():获取全部的键值对

+    Entry接口内部：
     +  getKey():获取key
       +  getValue():获取value

+    Hashtable特有方法
     + keys():获取全部key的枚举器
     + elements():获取全部value的枚举器

     这两个方法都是用Enumeration方式遍历
     
     > 作业：给定一个字符串，要求统计该字符串中每个字符出现的次数

#### 1.2 Hashtable

![](picture\Hashtable-结构图.png)

#### 1.3 HashMap与Hashtable区别

- Hashtable实现线程同步，多线程效率低，但安全，HashMp没有实现同步，多线程效率高，不安全
- Hashtable不允许键值对为null,但Hashmap的键和值都可为null
- Hashtable可以使用Enumeration方式遍历，Hashmap不可以（不掌握）

### 2.Properties

常用方法：

​	setProperties(String k,String v):添加键值对

​	getProperty(String k):通过key获取对应的value

​	load(输入流)：读取文件到properties中

​	store(输出流，评论):保存到文件中



### 3. LinkedHashMap

继承自HashMap,内部多了一个链表，用来保证添加的顺序与遍历顺序一致。

### 4.TreeMap

用红黑树实现的有序Map,TreeSet内部就是TreeMap.

## 6.Hashmap底层原理

**如果元素在hash之后散列的很均匀，那么table数组中的每个队列长度主要为0或者1**,但实际情况并非总是如此理想，虽然HashMap类默认的扩容因子为0.75，但是在数据量过大或者运气不佳时，还是会存在一些队列长度过长的情况，如果还是采用单向链表方式，那么查询某个节点的时间复杂度为O(n)；因此，对于一个桶超过8(默认值)的链表，jdk1.8中采用了红黑树的结构，那么查询的时间复杂度可以降低到O(logN)。

###6.1 原理图

+ 红黑树原理图

       1、每个节点不是红色就是黑色。
      2、根节点为黑色。
      3、如果节点为红色，其子节点必须为黑色。
      4、任意一个节点到到NULL（树尾端）的任何路径，所含之黑色节点数必须相同。

  https://www.cs.usfca.edu/~galles/visualization/Algorithms.html

![red-black](picture\red-black.png)

+ HashMap结构图

  ![hashMap_数据结构](picture\hashMap_数据结构.png)

### 6.2 调试模式查看方法的局部变量值：

+ 编译获得rt_debug.jar:参考下面文章产生

  https://blog.csdn.net/itarget/article/details/72785956



+ 添加到eclipse中
  + eclipse菜单windows-->preferences-->java-->Installed JREs
  + 选择右侧的jdk-->edits-->add External JARs-->选择rt_debug.jar
  + 在最后一行选择rt_debug.jar，点右侧的Up按钮使其提交位置到rt.jar之上

### 6.3构造方法

- new HashMap():设置加载因子为0.75，预设置未来数组table容量为16，但table并没有赋值


       transient Node<K,V>[] table;//数组属性的定义
        public HashMap() {
         this.loadFactor = DEFAULT_LOAD_FACTOR;
        } 
- new HashMap(5):加载因子为0.75,预设置未来数组table容量为8，但table并没有赋值

  8从哪来?
  tableSizeFor方法中，设置成大于等于5的2的最小次幂，就是8
### 6.4 put方法的过程

 ![hashMap_put](picture\hashMap_put.png)

+ 对key求hash值，然后再计算其对应的数组下标
+ 如果没有碰撞，就直接放入桶中。
+ 如果碰撞了，是链表，则放在链表尾，是红黑树，则加到红黑树中
+ 如果链表长度超过阈值，就把链表转为红黑树。
+ 如果key重复，则新值换旧值。
+ 如果键值对的数量超过(等于不处理)数组容量*0.75,则扩容

### 6.5 讲一下扩容的过程

+ 初始容量：
  + 构造中没指定容量：设置未来数组大小为16
  + 构造中指定容量n:设置未来数组大小为大于等于n的最小2的次幂。


+ 扩容：

  + 当前数组长度为n，添加一个元素后，当键值对数量大于(n*0.75)时，产生一容量为2n的新数组，旧数组中所有键值对的位置要么还在原始下标位置j，要么放到n+j处:  newTab[j + oldCap]
  + 添加一个元素到一个链表后，一个链表长度大于8，并且数组长度小于64，则也扩容


![HashMap元素转移](picture\HashMap元素转移.jpg)

### 6.6 思考题

+ 容量为什么必须是2的次幂？

  + 添加或查找元素时，元素放在数组的哪个下标处？

    是根据元素key的值跟数组长度取余得来，但%运算效率低(+-   >  * /   >   %).

  + HashMap是如何高效的取余得到下标呢？

    key的hashCode值hash跟数组的长度n之间进行下面按位与运算得到下标i的值。

    i = (n - 1) & hash，如果n为8，则n-1的二进制为111,运算结果刚好为取余,如果n不是2的次幂，则n-1的二进制就不能全是1，那么再按位取与就不是取余运算。

    如110,则数组长度为7，则此时再按位与运算的结果只能为000,010,100,110,也就是说在添加元素时，永远不可能放到下标为1,3,5这四处，导致hash算法不平均，为了平均，就必须用%跟7运算，这样运算效率又低，而hashMap并没有采用直接%运算。

+ 根据需求分析得知，一共只会有1000个元素，那么容量设置为多大？new HashMap(1000)合适吗？

  + 不合适，扩容非常消耗性能，所以尽量避免扩容。
  + 因为1000的话，数组初始容量为1024(2的10次方)，当元素个数达到1024*0.75=768时还会扩容，所以必须是大于1024的2的次幂2048


## 7. ConcurrentHashMap底层原理

### 1. 存储结构

底层采用数组+链表+红黑树的存储结构。

 ![ConCurrentHashMap架构图](picture\hashMap_数据结构.png)

### 2.容量计算

+ 构造中没指定大小：未来数组初始容量16

+ 构造中指定大小n：未来数组初始容量是  **必**  大于n的一个2的次幂，也不一定是最小的大于n的次幂，如指定为7，最终会变成16，而不是8。这点不同于HashMap的大于等于。

  initialCapacity + (initialCapacity >>> 1) + 1

+ sizeCtl的值不同，意义也不同

  + 正数：
    + 如果数组没初始化，则该值记录的是数组的预初始容量，
    + 如果数组已初始化，则该值记录的是数组将来扩容的阈值(容量*0.75)。
  + 0：表示数组还没有初始化，且数组的预设置容量为16
  + -1：表示数组正在初始化,则其他线程就不可以再对其初始化。
  + 小于-1,为-(n+1)：则表示数组正在扩容，且被n个线程扩容。

### 3. 数组初始化

+ sizeCtl<0: 表示数组正在初始化，再反复尝试初始化，直到table被初始化完毕，退出循环。
+ sizeCtl>=0: 采用CAS锁初始化数组。
  + CAS竞争失败，则反复尝试初始化，直到数组被初始化完毕，退出循环。
  + CAS竞争成功，则给sizeCtl赋值-1，阻止其它线程初始化。产生新数组，容量为计算出来的容量，如果构造中没指定则为16，最后把sizeCtl改为扩容的阈值，释放CAS锁。


       final Node<K,V>[] initTable() {
       Node<K,V>[] tab; int sc;
        while ((tab = table) == null || tab.length == 0) {
            if ((sc = sizeCtl) < 0)
                Thread.yield(); // lost initialization race; just spin
            else if (U.compareAndSwapInt(this, SIZECTL, sc, -1)) {
                try {
                    if ((tab = table) == null || tab.length == 0) {
                        int n = (sc > 0) ? sc : DEFAULT_CAPACITY;
                        @SuppressWarnings("unchecked")
                        Node<K,V>[] nt = (Node<K,V>[])new Node<?,?>[n];
                        table = tab = nt;
                        sc = n - (n >>> 2);//0.75n
                    }
                } finally {
                    sizeCtl = sc;
                }
                break;
            }
        }
        return tab;
    }
**总结：只有在执行第一次执行put方法时才会调用initTable()初始化数组，sizeCtl默认为0，如果ConcurrentHashMap实例化时有传参数，sizeCtl会是一个2的幂次方的值。所以执行第一次put操作的线程会执行Unsafe.compareAndSwapInt方法修改sizeCtl为-1，有且只有一个线程能够修改成功，其它线程通过Thread.yield()让出CPU时间片等待table初始化完成。**

### 4. 添加元素put

+ 键和值都不能为null

+ 添加第一个元素时，初始化数组，采用CAS思想修改sizectr变量为-1，保证其它线程不能同时初始化数组。compareAndSwapInt(this, SIZECTL, sc, -1)说明:

  比较参数2与参数3是否相等，相等就给参数2赋值为参数4，这是CAS乐观锁的思想:比较并交换, 用于保证并发时的无锁并发的安全性.

+ 数组已经初始化，根据key的hash与数组长度计算元素对应数组下标(桶)

  + 如果该位置没有元素，则采用cas思想向该位置添加元素，如果竞争失败，则重新添加，竞争成功，则把元素放在这个位置。map元素个数加1，结束添加。

  + 数组位置已经有元素，先对这个桶加锁，添加则要分两种情况

    + 该位置是链表(fh>=0,因为TreeBin中的hash小于0)：如果有相同的key，则替换旧值，没有相同的key则元素添加到尾部，元素添加后，如果该桶链表长度大于8则可能

      + 数组容量小于64则扩容
      + 大于等于64,则转换成红黑树，注意，整个红黑树不管有多少节点，就是一个完整的对象，加锁也是对这颗树加锁。

    + 该位置是红黑树：则在树结构上遍历元素，更新或增加节点。​



  **总结：如果table没初始化，则初始化，然后采用CAS+synchronized实现并发插入或更新操作**

  ​


### 5. 计算元素个数

- 查看size()方法得知：变量baseCount和数组counterCells协同记录元素的个数


- 当插入新数据或删除数据时，会通过addCount()方法更新baseCount和countercounterCells中的数据

- 如何使用countercounterCells数组？

  如果数组为空，就创建数组，修改该数组中某一元素中保存的值，如果多次cas竞争修改某一位置的计数都失败，会导致该计数数组的扩容。

下面方法仅了解

```
// x表示添加的key value的个数 默认1L，check用来检查是否要进行扩容
    private final void addCount(long x, int check) {
        CounterCell[] as; long b, s;
        // 如果 counterCells不为null 或者 设置basecount添加x失败
        if ((as = counterCells) != null ||
            !U.compareAndSwapLong(this, BASECOUNT, b = baseCount, s = b + x)) {
            CounterCell a; long v; int m;
            boolean uncontended = true;
            if (as == null || (m = as.length - 1) < 0 ||
                (a = as[ThreadLocalRandom.getProbe() & m]) == null ||
                !(uncontended =
                  U.compareAndSwapLong(a, CELLVALUE, v = a.value, v + x))) {
                fullAddCount(x, uncontended);//修改失败，则在方法中多次添加，如果还失败，就扩容该CounterCell数组。
                return;
            }
            if (check <= 1)
                return;
            s = sumCount();
        }
        // 如果check大于等于0，表示增加或修改了元素
        if (check >= 0) {
            Node<K,V>[] tab, nt; int n, sc;
            // 当s大于或等于sizeCtl(为table容量0.75),即满足扩容标准， 
            // 且table不为空。table的大小小于最大容量，且将sizeCtl赋值给了sc
            while (s >= (long)(sc = sizeCtl) && (tab = table) != null &&
                   (n = tab.length) < MAXIMUM_CAPACITY) {
                   // 根据table的大小生成一个标志戳，得出的是一个整数rs，高16bit是0 低16bit的第一个bit是1，也就是说，这个整数的第17比特位是1，
                int rs = resizeStamp(n);
                / sizeCtl < 0说明已经有线程将sizeCtrl设置为负数，并且正在进行数据迁移了。此时sizeCtrl的高16位就是，table数组容量的标志戳,代表着table的大小是否发生了变化，低16位记录着正在进行数据迁移的线程的数量。/
                if (sc < 0) {
                if ((sc >>> RESIZE_STAMP_SHIFT) != rs || sc == rs + 1 ||
                sc == rs + MAX_RESIZERS || (nt = nextTable) == null ||
                transferIndex <= 0)
                break;
      /如果上面的判断不成立，说明table数组扩容还没执行完，则执行这里的语句，即对SIZECTL进行+1， 如果执行到这里 进行+1 说明本线程将进行数据迁移，执行下面的transfer(tab, nt);/ 
                if (U.compareAndSwapInt(this, SIZECTL, sc, sc + 1))
                        transfer(tab, nt);
                }
    / 此时rs的低16位就是用来记录此时参与table扩容迁移数据时的线程数，此时SIZECTL<0，这也是为什么说只有第一个线程会进入这里。因为后续的线程再拿到SIZECTL值时 SIZECTL<0,会被if判断拦截，并开始执行下面的transfer()方法，开始数据迁移。 */        
                else if (U.compareAndSwapInt(this, SIZECTL, sc,
                                             (rs << RESIZE_STAMP_SHIFT) + 2))
                    transfer(tab, null);
                s = sumCount();
            }
        }



```

 

### 6. 扩容

+ 什么情况需要扩容？

  + 当某桶位链表节点数超过到8时，且数组总容量小于64，扩容为2的次幂

  + ```
    if (binCount >= TREEIFY_THRESHOLD)
       treeifyBin(tab, i);
    ----》
    if ((n = tab.length) < MIN_TREEIFY_CAPACITY)
       tryPresize(n << 1);
    ----》
    tableSizeFor(size + (size >>> 1) + 1);
    ```
  
  + 当添加元素成功后，元素数量达到容量阈值sizeCtl，进行扩容

![ConCurrentHashMap扩容](picture\ConCurrentHashMap扩容.png)

+ 如何扩容？

  + 首先根据运算得到需要遍历的位置i，然后利用tabAt方法获得i位置的元素f，初始化一个forwardNode实例fwd。
  + 如果f == null，则在table中的i位置使用CAS方法放入fwd
  + 如果f是链表的头或树节点，先用f上锁，把他们分别放在nextTable的i和i+n的位置上，移动完成，给table原位置赋值fwd。
  + 遍历过所有的节点以后就完成了复制工作，把table指向nextTable，并更新sizeCtl为新数组大小的0.75倍 ，扩容完成。


+ ### 在扩容时读写操作如何进行

  (1)对于get读操作，如果当前位置有数据，还没迁移完成，此时不影响读，能够正常进行。 
  如果当前链表已经迁移完成，那么头节点会被设置成fwd节点，此时get线程会帮助扩容。 
  (2)对于put/remove写操作，

  + 如果当前位置已经迁移完成，那么头节点会被设置成fwd节点，此时写线程会帮助扩容，然后在新表中增删
  + 如果当前位置扩容没有完成，当前链表的头节点会被锁住，所以写线程会协助扩容，在新表中增删除。 
  + 如果当前位置没有开始迁移，则获取锁，并在该位置增删，最后参与扩容。

  **总结：单个线程构建一个nextTable，通常大小为table的两倍，允许多线程并发把table的数据复制到nextTable中**

### 7. 读取数据

1. 判断table是否为空，如果为空，直接返回null。
2. 计算key的hash值，并获取指定table中指定位置的Node节点，通过遍历链表或则树结构找到对应的节点，返回value值。

### 8.面试问题：

#### 8.1 ConcurrentHashMap是如何保证并发安全的？

主要是利用Unsafe类的CAS乐观锁+Synchronized桶位锁实现

+ CAS:负责修改Map对象的属性或数组某个位置的数据。
+ Synchronized:主机负责在需要操作某个桶位时加锁，比如对链表或整个红黑树对象加锁

#### 8.2 什么时候链表转化为红黑树？

当`数组大小已经超过64`并且`链表中的元素个数超过默认设定（8个）`时，将链表转化为红黑树

#### 8.3如何统计元素个数?为什么这么做？ 

1. 使用baseCounter和数组里每个CounterCell的值之和。
2. 当多个线程同时执行CAS修改baseCount值，失败的线程会将值放到CounterCell中。所以统计元素个数时，要把baseCount和counterCells数组都考虑。

#### 8.4 **ConcurrentHashMap有什么优缺点？**

1. 优点：ConcurrentHashMap 是设计为非阻塞的。在更新时会局部锁住某部分数据，但不会把整个表都锁住。同步读取操作则是完全非阻塞的。好处是在保证合理的同步前提下，效率很高。
2. 缺点：**严格来说读取操作不能保证反映最近的更新**。例如线程A调用putAll写入大量数据，期间线程B调用get，则只能get到目前为止已经顺利插入的部分数据





# 九 Socket通信和NIO

##1.Socket通信

###1.1 原理

+ 连接：Server端要创建ServerSocket,用来监听某一个端口如999,当客户端创建socket对象时，就跟Server建立了连接
+ 通信：服务器端或客户端都可通过Socket对象获取输入流和输出流，通过输入流和输出流进行通信

### 1.2 实现

```
Server端
public static void main(String[] args) throws Exception {
		//1.创建ServerSocket对象,端口号范围在0~65535,用来监听某一个端口
		ServerSocket ss = new ServerSocket(999);
		System.out.println("服务器已启动...");
		while(true){
			//创建一个用于传递数据的套接字Socket
			Socket s = ss.accept();
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			System.out.println("等待客户端信息...");
			Thread.sleep(500);
			String str = br.readLine();
			System.out.println("来自客户端："+s.getInetAddress()+"的内容："+str);
			
			//向客户端发送信息
			PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s.getOutputStream())));
			pw.println("用户器已经收到你发来的信息："+str);
			pw.flush();
			s.close();
			br.close();
			pw.close();
		}		
	}
	
	
Clien端
public static void main(String[] args) throws Exception {
		//产生一个连接服务请求，并产生Socket对象
		Socket s = new Socket("192.168.1.118",999);
		//向服务器发送消息
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s.getOutputStream())));
		System.out.println("开始向服务器发送信息...");
		pw.println("socket通信中...");
		pw.flush();
		
		Thread.sleep(1000);
		System.out.println("等待接收服务器的消息...");
		BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
		System.out.println("来自服务器:"+s.getInetAddress()+"的信息："+br.readLine());
		
		s.close();
		br.close();
		pw.close();
	}
```

## 2. NIO

### 一、概述

NIO---Non Blocking IO,非阻塞IO

#### 1.IO与NIO区别

+ IO：面向单向流的
+ NIO：面向缓冲区，写数据时，把数据写入缓冲区中，然后再把缓冲区中数据通过管道传递到接收端。非阻塞和选择器则是针对网络通信使用

#### 2.NIO的两个技术

+ 缓冲区：用于装载数据，也就是数组，有7种类型的数据
  + ByteBuffer
  + CharBuffer
  + ShortBuffer
  + IntBuffer
  + LongBuffer
  + FloatBuffer
  + DoubleBuffer

+ 通道，channel:就是打开到IO设备的一个连接，通道用来传输数据

### 二、缓冲区与通道应用

### 1.直接缓冲区与非直接缓冲区的区别

直接缓冲区在数据读写时，会绕过JVM堆内存，速度快，因为少了一个中间的copy过程，效率高

直接缓冲区的缺点：建立和销毁比堆上缓冲区消耗大，不归jvm管理，不安全

![](非直接缓冲区.png)

### 2.缓冲区使用

+ put(数组)：向缓冲区中存放数据
+ limit:界限，表示缓冲区可操作的数据大小
+ position:位置，表示缓冲区中正在操作数据的位置
+ get():从缓冲区当前位置读一个数据
+ flip():对当前位置设置0，然后设置limit的值
+ mark():记录当前位置
+ reset():恢复到mark位置
+ hasRemaining()：判断是否还有数据
+ remaining():获取还有多少个数据
+ rewind();把position重置为0
+ clear():清空缓冲区，位置和界限恢复为初始状态
+ array():把缓冲区中数据转为数组

### 3.通道channel

#### 3.1 作用：

用于源节点与目标节点的连接，只用来跟buffer交互

#### 3.2 类型

+ FileChannel:用来操作本地文件
+ SocketChannel和ServerSocketChennel:用于 TCP数据传输
+ DatagramChannel:用于udp数据传输

### 4.分散与聚集

+ 分散：指从Channel中读取的数据依次分散到多个Buffer中
+ 聚集：将多个Buffer中的数据按照顺序写入到一个channel中

##三、非阻塞式应用

### 1.概念

+ 什么是阻塞？

程序在获取网络数据时，如果网络传输慢，就会一直等待，直到传输完成才不等待，这种等待叫阻塞

+ 什么是非阻塞？

  程序可直接获取已经准备就绪的数据，无需等待

+ 什么是NIO?

服务器的工作模式为一个请一个线程，也就是客户端发送的连接请求都会注册到选择器（多路复用器）上，多路复用器轮询到连接有IO请求时才启动一个线程进行处理。

+ 选择器：用于监控通道中io状况



# 十. 反射

## 1.概念

什么是反射：加载一个指定名称的字节码文件，从而产生class对象，通过class对象获取其中内容（属性、方法、构造方法）并调用，这种技术叫反射

class文件中有哪些内容？

​	有魔数字、类的版本、类的描述符，所实现的接口等等

class对象是什么？根据字节码文件的内容在内存区中产生的一个Class类型的对象

类对象什么时候产生？

+ 头一次创建类的实例对象，如new Person()
+ 访问类的静态方法或静态属性
+ 创建这个类的一个子类对象时
+ 通过反射主动加载并初始化
+ 启动虚拟机时，用户指定的主类，也就是包含main方法的类会先创建类对象

## 2.如何获取类对象

1. 类加载器之双亲委派模式

   ![反射-双亲委派模式](picture\反射-双亲委派模式.png)

   > + 加载过程
   >
   >   ```
   >   一个类加载器收到了类加载的请求，它首先不会自己去尝试加载这个类，而是把这个请求委派给父类加载器去完成，每一个层次的类加载器都是如此，因此所有的加载请求最终都应该传送到顶层的启动类加载器中，只有当父加载器反馈自己无法完全这个加载请求（它的搜索范围中没有找到所需的类）时，子加载器才会尝试自己去加载。
   >   ```
   >
   >
   > + 优点
   >
   >   ```
   >   用于保证Java程序的安全性
   >   如果没有使用双亲委派模型，如果用户可以自己编写了一个称为java.lang.Object的类,加入一些“病毒代码”,并通过自定义类加载器加入到JVM中,就可威胁到系统安全.
   >   ```
   >
   > 

2. 类加载的api方法

+ 通过Class类中静态方法forName来获取Class对象
  + 什么时候使用：只能获取该class文件所在路径时使用
+ 通过该类的实例对象的成员方法getClass()获取
  + 什么时候使用：当能获取其实例对象时使用

+ 通过类的class属性产生，但不会对类对象初始化
  + 什么时候用：当该类在当前工程中已经存在时才能使用
+ 通过类加载器获取，但不会对类对象初始化
  + 什么时候用：当需要产生类对象，但希望在使用时才初始化

## 3.获取class对象属性修饰符

+ getModifiers():获取类的描述符
+ getPackage():获取包声明
+ getName():获取包及类名
+ isInterface():判断是否是接口
+ getInterfaces():获取类所实现的接口的类对象

## 4.获取类的内容

### 4.1类的属性

+ 获取指定属性
  + getField(String name):只能获取当前类及父类中的public 权限属性
  + getDeclaredField(String name):只能获取当前类中所有指定的属性,但不包含父类属性

+ 获取全部属性
  + getFields():只能获取全部的public 权限属性
  + getDeclaredFields():只能获取当前类中所有的属性,但不包含父类属性
+ 访问属性
  + 设置值set(Object o,value)
  + setAccessible(true):权限不够时，强制访问
  + 获取值get(Object o)
  + 对于静态属性，对象o可写作null或写成类对象
  + 对于final型属性，无法修改其值（引用类型如数组可修改）

### 4.2类的方法

+ getMethod(String name,Class ...params)

+ getDeclaredMethod()

+ getMethods()

+ getDeclaredMethods()

+ 访问方法

  ```
  Method m = c.getDeclaredMethod("show",int.class,double.class,String.class);
  //m.setAccessible(true);权限不足时使用
  m.invoke(s, 4,5,"abc");//调用方法
  ```


### 4.3构造方法

+ newInstance():调用类中的无参构造方法产生实例对象

+ getConstructors():获取全部public 构造方法

+ getDeclaredConstructors():获取所有构造方法，包括私有

+ 如何产生实例对象：

  ```
  Constructor con = c.getDeclaredConstructor(String.class,int.class);
  con.setAccessible(true);
  s = (Stu) con.newInstance("丈夫在世当有为",12);
  ```


## 5.注解

### 5.1概念：

是代码里的特殊标记，可在编译、类加载、运行时被读取并执行相应的处理

作用：开发人员可使用注解在不改变原有逻辑的情况下，在源文件中嵌入一些补充信息

### 5.2注解的类型

+ jdk提供的常用注解

  + @Override:用来修饰方法，限定重写父类方法
  + @Deprecated:用于表示某个类或某个方法已过时
  + @SuppressWarnings:抑制编译器告警

+ 元注解

  用来标记其它注解的注解

  元注解的种类:

  + @Target:规定注解的作用域，也就是所修饰的注解可用在什么元素前，所取的值通过枚举

    ElementType来取值，如TYPE,FIELD,,METHOD

  + Retention:定义注解的生命周期

    + SOURCE:注解只能保留在源文件中
    + CLASS:注解在源文件和class文件中存在
    + RUNTIME:注解在运行期间可通过反射被读取和执行

## 5.3自定义注解

getDeclaredAnnotations():获取属性或方法上的全部注解

isAnnotationPresent(ServletConfig.class)：判断是否实现了指定类型的注解

getAnnotation(ServletConfig.class)：获取指定类型的注解

# 十 一. mysql数据库

## 1.概述

### 1.1mysql是什么？

是一种数据库管理软件

###1.2 什么是数据库？

是一种根据数据的结构来组织、管理、存储的一种数据仓库

###1.4目前主流数据库有哪些？

+ 微型数据库sqllite
+ 小型数据库：Access,大学实验室
+ 中型数据库mysql,sql-server
+ 大型 数据库DB2是IBM公司,oracle甲骨文,sybase赛贝斯

###1.5常用术语

+  DDL---Data Definition language

                                                                                	创建或删除数据库、数据表、修改表结构

                                                                                ​	一般用create 或drop或alter开头

+  DML---Data Manipulation language

                                                                                	对数据操作的语言，主要包括insert,delete,update

+  DQL---Data Query language

                                                                                	select

+  CURD:
   + C---create
   + U---update
   + R---read
   + D---delete

+  DCL---Data Control language 
   + 用户访问权限的控制
   + 事务

## 2.如何使用mysql

### 2.1连接数据库

+ 远程访问

  + use mysql

  + select host from user where user='root';

  + update user set host='%' where user='root';

  + flush privileges;

mysql -h 192.168.1.109 -uroot -p密码

## 3.常用命令

+ 查看数据库

  show databases;

  information_schema:存放一些系统的数据对象数据

  mysql:存放了用户权限相关信息

  test:是测试数据库，可以使用

+ 创建数据库

  **create database 名字;    create database if not exists lly;**

+ **删除数据库drop database 名字;   drop database if exists abc;**

+ 如果遇到汉字的乱码----set names gbk;


+ 复制表结构：

  + create table stu like t_stu;---只有表结构，没内容
  + create table stu2 select * from t_stu;---有表结构也有内容


+ 删除表drop table 表名

+ **查看表结构---desc 表名**;

+ **查看建表语句---show create table 表名**

+ **修改表名alter table 旧名 rename 新名;**

+ **修改列类型--alter table 表名 modify 列名 新类型;**

  + 示例：alter table stu modify name char(20);,当表中该字段有数据，且长度必须小于或等于20个字符才可修改。

+ **修改列名和列类型alter table 表名 change  旧列名 新名 新类型;**

+ **添加一列--alter table 表名 add 列名 列类型**

+ **删除一列--alter table 表名 drop 列名;**

+ 插入数据：**注意：oracle中不能一次插入多行**
  + insert into 表名 values(v1,v2,...vn),(v1,v2,...vn);
  + insert into 表名 (列名1，列名2，...列名n) values(v1,v2,...vn),(v1,v2,...vn);

+ 常用约束

  + not null---非空约束

  + default---如果没有赋值时，设置要取的默认值

  + primary key---主键约束，特点是非空且唯一

  + auto_increment---自增长

  + **unique**:唯一，可以为null

    create table emp (
    id int auto_increment,
    name char(32),
    sal double,
    sex char(2),
    constraint pk primary key(id)
    );
    
  + 外键约束foreign key references
  
    + 建表过程中添加约束--constraint 约束名 foreign key(列名) references 表名(主键名)

```
create table course(
id int primary key auto_increment,
cname char(32)
);

create table t_stu(
id int primary key auto_increment,
name char(32),
c_id int,
constraint myf foreign key(c_id) references course(id)
);
```



+ 约束管理

  + 添加约束

      建表后添加约束---

      + 外键：alter table 表名 add [constraint  约束名] foreign key(列名) references 表名(主键名)
      + 唯一：alter table stu add constraint abc unique(name);或alter table stu add unique(name);
      + 默认：alter table stu modify age int default 20;

  + 删除约束

    alter table 表名 drop 约束关键字 约束名;

    删除唯一约束：alter table 表名 drop index 约束名;

    删除主键：

    先去掉自增长：alter table stu change id it int;---》再删除主键alter table stu drop primary key;

    删除外键：alter table stu drop foreign key 外键名;

  + 查看约束

      select * from information_schema.table_constraints where table_name='stu';

  + 添加主键的自增长：alter table dept modify id int auto_increment;

  create table class(
  id int primary key auto_increment,
  name char(32) not null unique default 'java'
  );

   create table stu(
   id int auto_increment primary key,
   name char(32),
   class_id int,
   constraint fk_stu_t_class foreign key(class_id) references class(id)
   );

  //也可以不指定约束名

   create table stud(
   id int,
   t_id int,
   foreign key(t_id) references t_class(id)
   );

+ 创建索引：create index 索引名 on 表名(字段1，字段2。。。）;

## 4.数据类型

### 4.1数字类型

bit---只有0或1这两个数字

tinyint---1字节

smallint--2字节

mediumint---3字节

int---4字节

bigint---8字节

float

double

### 4.2 字符类型

char---用于定长字段，操作速度快，浪费空间

varchar---用于不定长字段，操作速度慢，节省空间

### 4.3日期类型

date---只有年月日

​	curdate()：获取当前日期,select curdate();

datetime---年月日时分秒,时间范围是从1000~9999

​	now()--获取当前日期和时间

timestamp:类同与datetime,范围是~1970~2038

练习：建表stu,有

​	学号id，长度为10

​	名字name,varchar,长度20

​	年龄age,int ,长度5，

​	生日birth,date

insert into stu values(1,'a1',23,'1990-07-09');

作业：创建三张表

```
dept表
  id主键
  部门名name

emp员工表
  工号id主键，自增长
  name
  所属部门号dept_id
  入职时间 joinTime
  工资sal
  津贴comm
  所属领导工号m_id

工资等级表salgrade
  id int,
  losal double
  hisal double


create table salgrade( 
  id int auto_increment primary key,
  losal double, 
  hisal double 
);
create table dept(
  id int(10) auto_increment primary key,
  name varchar(20) unique
);

create table emp(
  id int(10) auto_increment primary key,
  name varchar(20),
  dept_id int(10),
  joinTime date,
  sal int(10),
  bonus int(5),
  m_id int(10),
  constraint FK_dept foreign key(dept_id) references dept(id),
  constraint FK_emp foreign key(m_id) references emp(id)
);




向三张表中插入数据
insert into dept (name) values('人事'),('研发');

 insert into emp(name,dept_id,JoinTime,sal,bonus,m_id) values
 ('BOSS',1,'1999-01-01',20000,null,null),
 ('a1',2,'2015-4-4',5000,500,1);
 
 insert into salgrade values(1,0,1000),(2,1001,2000);
```

## 5.修改、删除、查询

### 5.1修改数据

update 表名 set 列1=值1，列2=值2,...列n=值n [where 条件]

示例：update class set name='c++' where id=3;

### 5.2删除数据

delete from 表名 [where 条件];

示例：delete from class where name='c++';

truncate class;----清空整张表，不能加条件，事务中不能回滚

### 5.3简单查询

select 列名1，列名2 from 表1，表2 where 条件;

## 6.多表设计

### 6.1  一对一关系

```
t1表内容
工号	姓名
1	 a1
2	 a2

t2表内容
身份证号	工资
1112		5000
2222		6000

对于这种关系的表，一般都把这两张表合并成一张
```

### 6.2   一对多关系 

```
t1表，如部门表
部门号	部门名
1		研发部
2		测试部

t2表，如员工表
工号	姓名	部门号
1	a1		1
2	a2		1
3	a3		2

对于一对多的关系，我们一般都会在多方表中增加一列，这一列作为外键，引用自单方表的主键
```

### 6.3	多对多关系

```
t1表 学生表
学号 	姓名
1		a1
2		a2
3		a3

t2表	课程表
课程号		课程名
1		java
2		ios
3		c++

此种情况，可建立一张中间关系表r,有如下列
id	学号	课程号
1	1		1
2	1		2
3	2		3
4	2		1
这里的学号和课程号都是引用自另外表中的主键
```

## 7.where条件

+ 比较

  + =
  + <
  + \>
  + \>=
  + <=
  + **<>**,!=

  示例：查询名字不为a1的学生信息

  select * from stu where name**<>**'a1';

+ 范围

  + between ... and 和not between... and

    示例：select * from emp where sal between 1000 and 4000;---查询出工资在[1000,4000]区间的所有员工

  + in(值1，值2,...)和not in()

    示例： select * from emp where sal in(1000,2000,3500,5500);

    select * from emp where sal not in(1000,2000,3500,5500);

+ 判断是否为空is null和is not null

  示例：select * from emp where sex is null;

  select * from emp where sex is not null;

+ 模糊查询like和not like

  + %:匹配任意多个任意字符，包括0个字符
  + _:匹配任意一个字符

  示例：select * from emp where name like 'a_';---首字符为a,一共两个字符

  ​	select * from emp where name like 'a%';---首字符为a，长度任意

  ​	select * from emp where name like '\_a\_';---一共3个字符，中间为a

  ​	select * from emp where name like '%a_';---长度大于等于2，倒数第二个为a

  ​	select * from emp where name like '%a%';包含a

+ distinct去除重复项

  示例：select distinct sal from emp;---查询有哪些工资

+ and/or

  示例：select * from emp where sal>5000 and name like '%a_';

  ​	select * from emp where sal>5000 or name like '%a_';

  ​	update emp set joinTime='2018-7-25' where id >3 and id<7;

  ​	select * from emp where joinTime>'2017';

  and优先级高于or

+ limit关键字

  select * from emp limit n---只显示结果集中的前n条记录

  select * from emp limit m,n---跳过结果集中的前m条记录，显示后面的n条记录

+ 别名：select class_id as 课程 from stu;

## 8常用函数

+   date_format(date,'%Y-%m-%d')：把日期转换为指定格式的字符串， oracle中用to_char()                                                                      


                                                                                    示例：查询2017年入职的员工select * from emp where date_format(joinTime,'%Y')='2017';
    
    + %Y---4位的年
    + %y---两位的年
    + %m---月份，格式为01~12
    + %c---月份,格式为1~12
    + %d---月份中的日期，格式01~31
    + %e---月份中的日期，格式1~31
    + %H---小时，00~23
    + %k---0~23
    + %h---00~11
    + %l---0~11
    + %i:分钟，00~59
    + %S:秒,00~59
    + %s:秒,0~59

+ str_to_date:把字符串转换为日期，oracle中用to_date

    查询在2017年8月以后入职的员工:select * from emp where joinTime>str_to_date('2017-8-1','%Y-%m-%d');


+ curdate():返回当前的日期

  + curtime():以HH:MM:SS格式返回当前时间
  + now():年月日时分秒格式返回当前时间
  + year():获取指定日期的年份,返回int

  面试题：在数据库中有一个入职日期，使用sql语句查出在公司服务了多少年？

  select year(curdate())-year(joinTime) "工 龄" from emp;


      	update emp set joinTime=curdate() where id=7;
      	insert into emp (joinTime) values(curdate());
+ ifnull(列名,替换值)：当所查询数据为null时，用替换值来替换该空的值


    select ifnull(joinTime,curdate()) from emp;
    select name,ifnull(m_id,'没有领导') 领导 from emp;
    可在查询时，给列启一别名，如:
    select ifnull(name,'没名字') as 姓名,ifnull(sex,'未知') "性 别" from emp;
+ if函数


      示例：select name,if(joinTime>'2017','较短','较长') from emp;
+ case表达式

  ```
  select name,sal,
   case
     when sal<5000 then '低'
     when sal>=5000 and sal<8000 then '中'
     else '高' 
  
    end   '工资等级'
     from emp;
  ```

+ 分组函数---重点

  当使用分组函数时，select 后面只能跟分组函数和分组条件,通过group by:指定分组条件

  + avg---平均值

  + max---最大值

  + min---最小值

  + sum---总和

  + count(1)或count(*):统计有多少行

    ​	示例：select count(*) from emp;

    count(列名):统计非null的有多少行

    示例：select count(sex) from emp;

  示例：mysql> select dept_id, sum(sal),avg(sal),max(sal),min(sal) from emp group by dept_id;

  select dept_id, count(1) from emp group by dept_id;统计每个部门多少人

## 9. 排序order by

​	升序:order by asc或order by

​	降序：order by desc

​	select * from emp order by sal desc;

一个查询中，如果有分组函数，where,group by,order by，执行顺序是

where过滤-->group by分组--->根据分组函数统计每一组的数据--->order by最后发生,

**所以where后面不能出现分组函数**,如果要对分组函数所统计出的结果进行过滤，则在group by后面

使用having,如：

查询那些在2017年以后入职的员工中，其所在部门号和所在部门的平均工资，只统计平均工资高于5000的部门，并按照平均工资降序排列。

select dept_id,avg(sal) from emp

where joinTime>'2017' 

 **group by** dept_id

 **having** avg(sal)>5000 

order by avg(sal) desc;

练习：

1.查询部门编号，部门平均工资，部门最高工资，部门最低工资，且部门平均工资高于2000

2.查询部门人数大于4的部门号，平均工资，部门人数

```
select dept_id, avg(sal), max(sal), min(sal), count(1) 
from emp
group by dept_id
having avg(sal)>2000 and count(1)>4;
```



## 10.多表查询

### 10.1笛卡尔积

也叫交叉连接，是多表查询的基础,后面的多表查询都是基于笛卡尔积的基础上进行过滤

select * from emp,dept;

### 10.2内连接

+ 隐式内连接

  查询每个员工姓名和部门名称

  select e.id, e.name,dept_id, d.name,d.id from emp e, dept d where dept_id=d.id;

+ 普通内连接

  select e.id, e.name,dept_id, d.name,d.id from emp e inner join dept d on  
  dept_id=d.id where d.name='研发';

练习：查询员工姓名，工资，和工资等级

select name,sal,s.id from emp e join salgrade s on e.sal>=s.losal and e.sal<s.hisal;

select name,sal,s.id from emp e , salgrade s where e.sal>=s.losal and e.sal<s.hisal;

在上面基础上添加员工所在部门名称

select e.name,sal,s.id,d.name 

from emp e , salgrade s,dept d 

where e.sal>=s.losal and e.sal<s.hisal and e.dept_id=d.id;

select e.name,sal,s.id,d.name
from emp e join salgrade s on e.sal>=s.losal and e.sal<s.hisal
join dept d on e.dept_id=d.id;	

在上面基础上添加业务条件：只查询1号部门的信息

select e.name,sal,s.id,d.name 

from emp e , salgrade s,dept d 

where e.sal>=s.losal and e.sal<s.hisal and e.dept_id=d.id 

and e.dept_id=1;

select e.name,sal,s.id,d.name 
from emp e join salgrade s on e.sal>=s.losal and e.sal<s.hisal 
join dept d on e.dept_id=d.id 

where d.id=1;





### 10.3.外连接

对于多表查询，内连接在表的关系过滤中，如果匹配不成功，则无法显示,如果要显示不能匹配的行，则要使用外连接

#### 10.3.1左外连接

​	关键字:left [outer] join on

查询每个员工姓名，部门号，部门名称

内连接实现：无法查询出没有部门号的员工信息

 select e.name,dept_id,d.name
 from emp e,dept d
 where e.dept_id=d.id;或

select e.name,dept_id,d.name from emp e join dept d on e.dept_id=d.id;

外连接实现:

select e.name,dept_id,d.name  
from emp e left join dept d on e.dept_id=d.id;

查看每个部门有哪些员工

select d.name,e.name from dept d left join emp e on dept_id=d.id;

查询每个部门名和其部门人数

select d.name, count(e.id)  

from dept d left join emp e on e.dept_id=d.id 
group by d.name;

#### 10.3.2右外连接

同样实现上面功能

 select e.name,d.id,d.name
 from dept d right join emp e on d.id=e.dept_id;

select d.name, count(e.id)  

from emp e right join dept d on e.dept_id=d.id 
group by d.name;

#### 10.3.3 全连接

把左连接与右连接合并

应用案例：

+ 查询全部没有被选修的课程信息和没有学习任何课程的学生信息。

  ```
  create table course(
  c_id int primary key auto_increment,
  name char(32)
  );

  create table stu(
   s_id int primary key auto_increment,
   c_id int,
   sname char(32),
   score double,
   constraint foreign key(c_id) references course(c_id)
   );
  insert into course (name) values('java'),('c++'); 
  insert into stu (c_id,sname,score) values
  (1,'s1',78),(null,'s5',null);
   

  select *
  from course c left join stu s 
  on c.c_id=s.c_id
  where s.s_id is null;
  ```

+ 还没有学习任何课程的学生信息信息和对应课程信息。

  ```
  select * from course c right join stu
  on c.c_id=stu.c_id
  where stu.c_id is null;
  ```

+ 把这两个需求合并

  ```
   select *
   from course c left join stu s
   on c.c_id=s.c_id
   where s.s_id is null 
   union
   select * from course c right join stu
   on c.c_id=stu.c_id
   where stu.c_id is null;
  ```

#### 10.3.4 练习：

stu

s_id	 c_id  sname	 score

course

c_id	name

1.查询每个学生的姓名和学习的课程名字

```
 select sname,cname
 from stu,course
 where stu.c_id=course.id;
 
  select sname,cname
 from stu join course
 on stu.c_id=course.id;
 
 外连接
 select sname,cname
 from stu left join course
 on stu.c_id=course.id;
```

2.统计每门课程的人数

```
select cname,count(c_id)
 from stu right join course
 on stu.c_id=course.id
 group by cname;
```

3.统计每门课程的平均成绩，并按课程名排序

```
 select cname,avg(score)
 from course left join stu
 on course.id=stu.c_id
 group by cname
 order by cname;
```



### 10.4子查询

#### 10.4.1 where型子查询

+ 单行子查询

查询工资高于平均工资的员工

select * from emp where sal>(select avg(sal) from emp);

实现查询平均成绩高于学校平均成绩的课程名称

解题思路：

+ 根据需求获取select后面要写的内容
+ 根据select后面内容和过滤条件分析出要使用到哪些表，写在from 后面
+ 分析这些表之间的关系，确立表关系的过滤条件
+ 根据查询信息和条件分析是否要分组
+ 分析是否要对分组后的信息进行having过滤
+ 分析是否要排序

select cname,avg(score) 

from course,stu 

where course.id=stu.c_id 

group by cname 

having avg(score)>(select avg(score) from stu);

+ 多行子查询：

  查询平均工资大于1300的部门名称

  ```
  内连接实现
  select dname ,avg(salary) from emp e,dept d
  where e.dept_id=d.id
  group by dname
  having avg(salary)>1300;
  
  子查询实现
  select dname from dept
  where id in
  (select dept_id from emp group by dept_id having avg(salary) > 1300);
  
  ```

#### 10.4.2 from型子查询

查询工号大于3的员工中，工资高于这部分员工的平均工资的员工姓名和工资

工号大于3---select name,sal from emp where id>3;

 select * from (select name,sal  from emp where id>3)tmp
 where sal>(select avg(sal) from emp where id>3);

#### 10.4.3 select型 子查询

查询每个部门人数

```
内连接
select d.name,count(e.id)
from dept d,emp e
where d.id=e.dept_id
group by d.name;

子查询
 select d.*, (select count(*) from emp e where e.dept_id=d.id) '人数'
 from dept d;
```

练习：

```
1.查询所有人所属的部门名称和员工名称？
2.查询部门平均工资高于1000的所有部门id
3.统计部门的平均工资大于公司平均工资的部门
4.查询出平均工资小于1300的部门名称
5.查询出没有津贴的员工所在的部门名称。
6.查询所有员工作息，并显示出其领导的名字。
7.查询所有员工信息，并显示出他领导的名字及领导所属的部门名称。
8.查出每个部门的平均工资，并显示出部门的名称。
9.查出工资比平均工资高的员工姓名和他所属的部门名称。
10.查询下表中出各项成绩都不低于80的同学的姓名。
```

## 11. 事务

###1.概念

用户定义的一系列数据库操作，这些操作可以视为一个完整的单元，要么全部执行，要么全部不执行，是不可分割的工作单元。

### 2.特点（ACID）

​	1.原子性atomicity.--事务是一个不可分割的单位，事务中操作要么全成功，要么全失败
​	2.一致性consistency--事务对数据的影响只应该有两个结果：

​				a.事务结束后，数据的状态跟开始时一致。

​				b.事务结束后，数据的状态跟预期结果一致。

​	3.隔离性isolation--事务之间不能相互干扰，即一个事务内部的操作及使用的数据对并发的其他事务之间不能相互干扰。ps:这只是一种理想情况，实际上不能完全作到.
​	4.持久性duration--事务操作完毕后就永久的保存到数据库,接下来的其他操作对其不应该有影响，

​		> 通过写入redo log方式实现

###3.事务命令

+ 开启事务：start transaction或begin;
+ 回滚事务：rollback;
+ 提交事务： commit;

### 4.事务的三个问题

+ 脏读：一个事务读取到另一个事务没提交的数据。
+ 不可重复读：一个事务范围内，多次查询某个数据，却得到不同的结果
  + A事务先读取数据
  + B事务修改了该数据，并提交。
  + A再次读取该数据就跟上一次不一致了。
+ 幻读或虚读
  + A事务已经读取数据
  + B事务添加或删除一些行的数据，并提交
  + A事务如果再次读取时，就会多或少几行数据，强调的是由于增删操作导致数据行数的前后不一致

+ 事务的隔离级别：

  + 查询隔离级别：select @@tx_isolation;

  1. read_uncommitted:读未提交，会脏读、不可重复读，幻读，set session transaction isolation level read uncommitted;

  2. read_committed:读已提交，是oracle默认隔离级别，只能解决脏读。oracle默认级别

     set session transaction isolation level read committed;

  3. repeatable read:可重复读，解决了脏读，不可重复读，但偶尔会幻读，是mysql默认隔离级别。

     set session transaction isolation level repeatable read;

  4. serializable: 串行化，事务是一个接着一个串行执行，则保证了不会幻读，不会发生不可重复读，不会脏读，最安全，效率最低。

     set session transaction isolation level serializable;

## 12.数据库锁



###1.概念：

java代码中可加锁，但不彻底，因为在集群环境下，服务器1和2中虽然都对同样代码加锁，只在一个服务器中是串行，对于数据库来说却是两个并发执行

### 2.表锁：

####2.1 读锁
lock table test read;---给test 表加表锁,此时**任何线程**都不能再对test表增删改,但都可读,同时当前窗口也不能操作其他表.
unlock tables---解锁

####2.2 写锁
lock table test write;--此时其他线程不能对该表读和写，当前线程(cmd客户端窗口)可读可写,同时当前窗口与不能操作其他表
 特点：一般不用表锁，影响性能，不会死锁

###3.行锁

####3.1 独占锁，排他锁

当一个事物加入排他锁后，当前线程可读可写，不允许其他事务加共享锁或者排它锁读取，更加不允许其他事务修改加锁的行，但可以读

A窗口
set autocommit=0;---关闭自动提交,也就是开事务，可用begin;
update test set num=120 where id=1;把这一行加了锁，在提交之前，其他线程只能读，不能写这一行
select * from test where id=1 for update;---作用同上，区别：读出数据后，在事务中根据数据本身进行某种修改，而上面只为修改
注意：如果where后面的条件字段如果没有索引，则相当于整张表加索

####3.2.数据库死锁
表锁不会死锁，只有行锁会发生
A窗口
set autocommit=0;
select * from test where id=1 for update;
B窗口
set autocommit=0;
select * from test where id=2 for update;

A窗口
update test set num=50 where id=2;---等待....

B窗口
update test set num=50 where id=1;---报死锁错误，并让A窗口提交,如何避免？对一行加锁后，就只修改这一行，不要修改其他行

####3.3 共享锁

特点：

- 允许其它事务也增加共享锁读取，但不能加排它锁
- 当事务同时增加共享锁时候，事务的更新必须等待先执行的事务 commit 后才行，如果同时并发太大可能很容易造成死锁

A窗口 

+ begin
+ select * from test where id=1 lock in share mode;---加行读锁，此时A窗口可读可写，其它线程可读不可写

如果两个窗口都对同一行加共享锁，可能会死锁，如
先把上面事务提交，分别在A和B窗口

+ begin
+ select * from test where id=1 lock in share mode;
+ 分别在两窗口执行
  update test set num=70 where id=1;---死锁,如何避免？如果加共享锁就不要写，只用来读



# 十二、jdbc

## 1.jdbc是什么？

是java中jdk的一部分，java访问数据库必须通过jdbc所提供的工具实现

## 2.访问数据库步骤

+ 注册：Class.forName("com.mysql.jdbc.Driver");
+ 产生数据库的连接：DriverManager.getConnection(url, user, pass)
+ 产生Statement:
  + con.createStatement()
  + con.preparedStatement(String sql);
    + setXxx(参数的下标，参数的值)
    + setObject(参数的下标，参数的值)
+ 操作数据库:
  + 对数据库进行增删改：st.executeUpdate(sql)
  + 对数据库进行查询：st.executeQuery(sql)
    + 返回值rs为ResultSet类型的结果集
    + rs.next():完成两件事：1.把游标移动到下一行；2.判断当前位置是否有数据
    + rs.getXxx(下标或列名):获取当前位置对应字段的数据；rs.getObject(下标或列名)：不管什么类型都可以使用，但用的不多



## 3.Statement与PreparedStatement区别

+ 前者要用字符串拼接来实现参数的传入，而后者采用占位符的方式传递参数，所以后者简化了处理

+ 前者没有预处理功能，而后者有预处理功能,但要在url中添加参数才能开启：?useServerPrepStmts=true

  + Statement:对于每一个sql语句都会在数据库中执行语法检查并编译后才执行

  + PreparedStatement:对于相同结构的sql语句，在数据库中只会被编译一次，如果一种结构的sql语句使用率高时，就用PreparedStatement,而如果很少使用时，可以就用Statement


+ 防止sql注入:对于Statement,因为是采用字符串拼接的方式产生的sql语句，所以如果字符串中

  包含了一些特殊符号时可能会改变sql语句的结构，从而破坏sql语句原本功能，这种情况叫sql注入

## 4.批处理

### 4.1 Statement

+ st.addBatch(sql):添加到批处理中
+ st.executeBatch():执行批处理

### 4.2 PreparedStatement

+ st.addBatch():在填充完参数后添加批处理
+ st.executeBatch():执行批处理

## 5.数据库连接池

概念：是一个容器，用于存放多个Connection对象，在使用连接时，从容器中获取，使用完毕后，再通过con.close()将这个con重新放入容器中，该容器就是连接池

作用：可通过连接池获取连接对象，节省创建和释放连接所消耗的性能，连接池中对象可被重复使用

## 6.jdbc模板

在使用模板时，如果是增删改操作，就直接调用update(String sql,Object...args)

如果是查询操作，就要实现接口Rowmapper，并实现方法mapRow方法，返回当前行的数据所映射的对象

## 7.事务使用



+ 开启事务con.setAutoCommit(false);
+ 把组成这个事务的操作放在一个try语句块中,最后一行用con.commit():
  + 执行redo log,内容比如：insert into user values(1,'a1');
+ catch分支中con.rollback();执行undo log,内容比如：
  + delete from user where id=1;
+ con.setAutoCommit(true);

## 8.获取主键值

+ executeUpdate(sql, Statement.RETURN_GENERATED_KEYS)
+ ResultSet rs = st.getGeneratedKeys();
+ rs.getInt(1)

## 9.对象工厂

原理：采用反射技术，通过类的路径来产生对象，在项目中需要对象时，向对象工厂获取

作用：可降低代码耦合度

## 10.静态代理

作用：**可以在不修改目标对象的基础上，通过扩展代理类，进行一些功能的附加与增强**

如何实现：需要定义接口或者父类,被代理对象与代理对象一起实现相同的接口或者是继承相同父类

## 11.三层架构

执行sql文件用source 把文件拖入mysql中

把项目分为三层：

+ 视图层:直接跟用户交互的图形界面，可用如下技术实现：

  swing, html,javascript,css,vue....


+ 业务层：实现业务过滤和判断等。


+ dao层： 

void insertUser(Stu s){

​	st.executeUpdate()

}

List selectUserByName(String name){

}

 ![三层架构](picture\三层架构.jpg)

## 12.元数据

通过数据库的连接或ResultSet获取数据库的信息

DatabaseMetaData:数据库元数据,

​	获取方式：con.getMetaData()

ResultSetMetaData:结果集元数据

​	获取方式：rs.getMetaData()



# 十三. 面试题汇总

## 1.String能否继承，和stringbuffer stringbuilder区别 

## 2. 产生对象的方式有哪些？

- 调用构造方法
  - 使用new关键字
  - 使用Class类的newInstance方法
  - 使用Constructor类的newInstance方法
- 不调用构造方法
  - 使用clone方法
  - 使用反序列化

## 3.继承、聚合与组合的区别

- public class A extends B { }   ---B继承自A
- public class A{ List<B> b} A可以有b  ----聚合
- public class A { B b} A一定有b   ---组合

## 4. 线程

+ 线程池

核心参数, 线程池中的等待时间有什么作用, 线程池中的拒绝策略有哪些

## 5. jdbc

+ Pst和st对比     callablestatement干啥用的，    你们用存储过程吗

## 6. Socket通信

写一个服务端和客户端

## 7. 算法

+ 冒泡排序

+ 处理数组

  ```
  （1）定义一个一维数组int arr[]={12,24，18,20}把数组里的数据写入某个文件中并读出来，在按倒序的方式输出；例如： arr输出的结果是{20,18,24,12}
  ```

+ 红黑树









